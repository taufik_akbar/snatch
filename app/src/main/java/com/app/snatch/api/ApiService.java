package com.app.snatch.api;

import com.app.snatch.model.AddSnatchRequest;
import com.app.snatch.model.request.LoginRequest;
import com.app.snatch.model.response.DefaultResponse;
import com.app.snatch.model.response.InboxResponse;
import com.app.snatch.model.response.LocationResponse;
import com.app.snatch.model.response.OwnedResponse;
import com.app.snatch.model.response.ProfileResponse;
import com.app.snatch.model.response.SnatchDetailResponse;
import com.app.snatch.model.response.StaticResponse;
import com.app.snatch.model.response.UserAccessResponse;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.app.snatch.api.ApiConfig.EDIT_PROFILE;
import static com.app.snatch.api.ApiConfig.INBOX;
import static com.app.snatch.api.ApiConfig.LOGIN;
import static com.app.snatch.api.ApiConfig.OWNED_SNATCH;
import static com.app.snatch.api.ApiConfig.PROFILE;
import static com.app.snatch.api.ApiConfig.SNATCH_ADD;
import static com.app.snatch.api.ApiConfig.SNATCH_CREATE;
import static com.app.snatch.api.ApiConfig.SNATCH_LIST;
import static com.app.snatch.api.ApiConfig.STATIC;

/**
 * Created by taufik on 11/27/17.
 */

public interface ApiService {
    
    @POST(LOGIN)
    Observable<UserAccessResponse> postLogin(@Body LoginRequest loginRequest);
    
    @POST(PROFILE)
    Observable<ProfileResponse> getProfile(@Header("Authorization") String token);
    
    @GET(STATIC)
    Observable<StaticResponse> getStatic(@Header("Authorization") String token);
    
    @GET(OWNED_SNATCH)
    Observable<OwnedResponse> getOwned(@Header("Authorization") String token);
    
    @GET(SNATCH_LIST)
    Observable<OwnedResponse> getSnatchList(@Header("Authorization") String token);
    
    @GET(INBOX)
    Observable<InboxResponse> getInbox(@Header("Authorization") String token);
    
    @POST(SNATCH_ADD)
    Observable<DefaultResponse> addSnatch(@Header("Authorization") String token, @Body AddSnatchRequest requestBody);
    
//    api/snatch/list/2/1/5
    @GET("/api/snatch/list/{type_id}/{start}/{limit}")
    Observable<OwnedResponse> getCategoryDataList(@Header("Authorization") String token,
                                                  @Path("type_id") String typeId,
                                                  @Path("start") String page,
                                                  @Path("limit") String limit);
    
    @GET("/api/snatch/detail/{id}")
    Observable<SnatchDetailResponse> getSnatchDetail(@Header("Authorization") String token,
                                                     @Path("id") String snatchId);
    
    @POST(SNATCH_CREATE)
    Observable<DefaultResponse> createSnatch(
            @Header("Authorization") String token,
            @Body RequestBody file);
    
    @POST(EDIT_PROFILE)
    Observable<DefaultResponse> editProfile(
            @Header("Authorization") String token,
            @Body RequestBody file);
    
    @GET("json")
    Call<LocationResponse> getLocation(@Query("components") String postalCode, @Query("result_type") String resultType, @Query("key") String apikey);
}
