package com.app.snatch.api;

/**
 * Created by taufik on 11/27/17.
 */

public interface ApiConfig {
    String BASE_URL_IMAGE = "https://project.kotakpasir.id";
    String BASE_URL = "https://project.kotakpasir.id/api/";
    String MODUL_API = "api/";
    String MODUL_AUTH = BASE_URL + "auth/";
    String MODUL_USER = BASE_URL + "user/";
    
    String LOGIN = MODUL_AUTH + "login";
    String PROFILE = MODUL_USER + "profile";
    String STATIC = "static";
    String OWNED_SNATCH = MODUL_USER + "snatch/owned";
    String SNATCH_LIST = MODUL_USER + "snatch/list";
    String INBOX = MODUL_USER + "inbox";
    String SNATCH_ADD = "snatch/add";
    String SNATCH_CREATE = "snatch/create";
    String EDIT_PROFILE = MODUL_USER + "update";
    String INFO_PROFILE = MODUL_USER + "profile";
}
