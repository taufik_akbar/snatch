package com.app.snatch.injection;

import android.app.Application;
import android.os.StrictMode;

import com.app.snatch.R;
import com.app.snatch.utils.Constants;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Taufik Akbar on 19/12/2016.
 */

public class BaseApp extends Application implements Constants {
    
    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        FirebaseApp.initializeApp(this);
//        Realm.init(this);
//        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
//                .name("fmuslimdb.realm")
//                .build();
    
//        Realm.setDefaultConfiguration(realmConfiguration);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/Lato-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        appComponent =  DaggerAppComponent.builder().appModule(new AppModule(this))
                .networkModule(new NetworkModule()).build();
        appComponent.inject(this);
    }


}
