package com.app.snatch.injection;


import com.app.snatch.presenter.AddSnatchPresenter;
import com.app.snatch.presenter.CreateSnatchPresenter;
import com.app.snatch.presenter.DetailSnatchPresenter;
import com.app.snatch.presenter.EditProfilePresenter;
import com.app.snatch.presenter.InboxPresenter;
import com.app.snatch.presenter.LoginPresenter;
import com.app.snatch.presenter.ProfilePresenter;
import com.app.snatch.presenter.SnatchListPresenter;
import com.app.snatch.presenter.SnatchOwnedPresenter;
import com.app.snatch.presenter.StaticPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Taufik Akbar on 19/12/2016.
 */


@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(BaseApp app);
    void inject(LoginPresenter loginPresenter);
    void inject(ProfilePresenter profilePresenter);
    void inject(StaticPresenter staticPresenter);
    void inject(SnatchOwnedPresenter snatchOwnedPresenter);
    void inject(InboxPresenter inboxPresenter);
    void inject(SnatchListPresenter snatchListPresenter);
    void inject(DetailSnatchPresenter detailSnatchPresenter);
    void inject(AddSnatchPresenter addSnatchPresenter);
    void inject(CreateSnatchPresenter createSnatchPresenter);
    void inject(EditProfilePresenter editProfilePresenter);
    

}
