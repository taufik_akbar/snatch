package com.app.snatch;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.snatch.model.User;
import com.app.snatch.utils.Constants;


/**
 * Created by taufik on 5/1/17.
 */

public class UserPreference implements Constants {
    

    
    private final static String USER_ID = "USER_ID";
    private final static String USER_NAME = "USER_NAME";
    private final static String USER_CEA = "USER_CEA";
    private final static String USER_DESIGNATION = "USER_DESIGNATION";
    private final static String USER_PIC = "USER_PIC";
    private final static String USER_TOKEN = "USER_TOKEN";
    private final static String USER_GENDER = "USER_GENDER";
    private final static String FCM = "FCM";
    
    public static void setUser(Context context, User user) {
        
        SharedPreferences prefs = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = prefs.edit();
        mEditor.putString(USER_ID, user.getUserId());
        mEditor.putString(USER_NAME, user.getName());
        mEditor.putString(USER_CEA, user.getCea());
        mEditor.putString(USER_DESIGNATION, user.getDesignation());
        mEditor.putString(USER_PIC, user.getImagePath().getImage());
//        mEditor.putString(USER_TOKEN, user.getToken());
        mEditor.putString(USER_GENDER, user.getGender());
//        mEditor.putString(USER_ADDRESS, user.getToken());
        mEditor.apply();
    }
    
    public static User getUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        User user = new User();
        user.setUserId(prefs.getString(USER_ID, DEFAULT_STRING));
        user.setName(prefs.getString(USER_NAME, DEFAULT_STRING));
        user.setCea(prefs.getString(USER_CEA, DEFAULT_STRING));
        user.setDesignation(prefs.getString(USER_DESIGNATION, DEFAULT_STRING));
        user.setGender(prefs.getString(USER_GENDER, DEFAULT_STRING));
//        user.getImagePath().setImage(prefs.getString(USER_PIC, DEFAULT_STRING));
        return user;
    }
    
    public static void setFcm(Context context, String fcm) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = prefs.edit();
        mEditor.putString(FCM, fcm);
        mEditor.apply();
    }
    
//    public static String getFcm(Context context) {
//        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
//        return preferences.getString(FCM, DEFAULT_STRING);
//    }
//
    public static String getUserId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        return preferences.getString(USER_ID, DEFAULT_STRING);
    }
    
    public static String getUserGender(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        return preferences.getString(USER_GENDER, DEFAULT_STRING);
    }
    
    public static String getName(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        return preferences.getString(USER_NAME, DEFAULT_STRING);
    }
    
    public static String getCea(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        return preferences.getString(USER_CEA, DEFAULT_STRING);
    }
    
    public static String getDesignation(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        return preferences.getString(USER_DESIGNATION, DEFAULT_STRING);
    }
    
    public static String getProfPic(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        return preferences.getString(USER_PIC, DEFAULT_STRING);
    }
//
//    public static String getUserEmail(Context context) {
//        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
//        return preferences.getString(USER_EMAIL, DEFAULT_STRING);
//    }
//
//    public static String getUserPhone(Context context) {
//        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
//        return preferences.getString(USER_PHONE, DEFAULT_STRING);
//    }
//
//    public static String getUserPic(Context context) {
//        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
//        return preferences.getString(USER_PIC, DEFAULT_STRING);
//    }
    
    public static void setToken(Context context, String token) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = prefs.edit();
        mEditor.putString(USER_TOKEN, token);
        mEditor.apply();
    }
    
    public static String getToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        return preferences.getString(USER_TOKEN, DEFAULT_STRING);
    }
    
    public static void signout(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = preferences.edit();
        mEditor.clear();
        mEditor.apply();
    }
    
//    public static String getNpwp(Context context) {
//        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
//        return preferences.getString(USER_NPWP, DEFAULT_STRING);
//    }
//
//    public static String getAddress(Context context) {
//        SharedPreferences preferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
//        return preferences.getString(USER_ADDRESS, DEFAULT_STRING);
//    }
}
