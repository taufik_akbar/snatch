package com.app.snatch.utils;

/**
 * Created by taufik on 11/27/17.
 */

public interface Constants {
    
    String NO_CONNECTION = "Internet connection not available";
    
    String PREF_USER = "PREF_USER";
    String PREF_LOCATION = "PREF_LOCATION";
    
    int DEFAULT_INT = 0;
    String DEFAULT_STRING = "";
    String DEFAULT_STRING_0 = "0";
    
    String TYPE = "TYPE";
    String TYPE_SNATCH = "TYPE_SNATCH";
    String MY_SNATCH = "MY_SNATCH";
    String OWNED_LIST = "OWNED_LIST";
    String DRAFT = "DRAFT";
    String HOME_SNATCH = "HOME_SNATCH";
    
    String LATEST_LISTING = "Latest Listing";
    String COMMERCIAL = "Commercial";
    String RESIDENTIAL = "Residential";
    String AMENITIES = "AMENITIES";
    String PROPERTY_NEARBY = "PROPERTY_NEARBY";
    String STATIC_RESPONSE = "STATIC_RESPONSE";
    String STATIC_REQUEST = "STATIC_REQUEST";
    String SNATCH_DATA = "SNATCH_DATA";
    String STATIC_DATA = "SNATCH_DATA";
    String VIEW_AMENITIES = "VIEW_AMENITIES";
    String VIEW_NEARBY = "VIEW_NEARBY";
    String DISTRICT = "DISTRICT";
    String TITLE = "TITLE";
    
    
    String COMMERCIAL_RENT = "COMMERCIAL_RENT";
    String COMMERCIAL_SALE = "COMMERCIAL_SALE";
    
    String FOR_RENT = "for rent";
    String FOR_SALE = "for sale";
    String SALE = "sale";
    String RENT = "rent";
    String HDB = "hdb";
    String PRIVATE = "private";
    String CONDO = "condo";
    String LANDED = "landed";
    
}
