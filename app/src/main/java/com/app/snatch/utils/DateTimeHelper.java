package com.app.snatch.utils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by taufik on 12/19/17.
 */

public class DateTimeHelper {
    
    public static String getTime(String time) {
        long millisecond = Long.parseLong(time);
        // or you already have long value of date, use this instead of milliseconds variable.
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
    
        return dateFormat.format(new Date(millisecond));
    }
}
