package com.app.snatch.utils;

import java.io.IOException;

public class ErrorHelper implements Constants {

    /**this method to show presenter error response http like 403 or 404*/
    public static String responseSuccessError(int errorCode, String errorMessage) {
        if (errorMessage != null) {
            return "HTTP " + errorCode + " " + errorMessage;
        }
        return "Unknown error";
    }

    /**this method to show presenter error response if connection not available*/
    public static String responseFailedError(Throwable e) {
        if (e != null) {
            if (e instanceof IOException) {
                return NO_CONNECTION;
            } else  {
                return e.getMessage();
            }
        }
        return "Unknown error";
    }

    /**this method to show rx presenter error response if connection not available*/
    public static String rxError(Throwable e) {
        if (e != null) {
            if (e instanceof IOException) {
                return NO_CONNECTION;
            } else  {
                return e.getMessage();
            }
        }
        return "Unknown error";
    }
}