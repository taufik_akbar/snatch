package com.app.snatch.utils;

import java.util.ArrayList;

/**
 * Created by taufik on 12/13/17.
 */

public class Dummy {
    
    public static ArrayList<String> arrayAmenities() {
        
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Swimming Pool");
        arrayList.add("Basement Parking");
        arrayList.add("Parking");
        arrayList.add("CCTV/Security");
        arrayList.add("Gym");
        arrayList.add("Fridge");
        arrayList.add("Jacuzzi");
        arrayList.add("Closet");
        arrayList.add("Balcony");
        arrayList.add("BBQ Pits");
        arrayList.add("Stove");
        arrayList.add("Air Conditioner");
        arrayList.add("Washer");
        arrayList.add("Dryer");
        arrayList.add("Playground");
        
        return arrayList;
    }
    
    public static ArrayList<String> arrayProperty() {
        
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Commute");
        arrayList.add("MRT Stations");
        arrayList.add("School");
        arrayList.add("Post Office");
        arrayList.add("ATM");
        arrayList.add("Supermarket");
        arrayList.add("Clinic");
        arrayList.add("Bus Stop");
        arrayList.add("Bank");
        arrayList.add("Town Park");
        
        return arrayList;
    }
    
    public static ArrayList<String> arraySubtype() {
        
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("3A");
        arrayList.add("3B");
        arrayList.add("4C");
        
        return arrayList;
    }
    
    public static ArrayList<String> arrayCategory() {
        
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Commercial");
        arrayList.add("Residential");
        
        return arrayList;
    }
    
    public static ArrayList<String> arrayPropertyGrouptype() {
        
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("HDB Apartement");
        arrayList.add("BAA Apartement");
        arrayList.add("CAA Apartement");
        
        return arrayList;
    }
}
