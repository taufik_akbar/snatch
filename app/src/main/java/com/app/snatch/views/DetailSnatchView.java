package com.app.snatch.views;


import com.app.snatch.model.SnatchDetail;

/**
 * Created by Taufik Akbar on 02/03/2017.
 */

public interface DetailSnatchView extends BaseView {
    
    void onDetailFail(String message);

    void onDetailSuccess(SnatchDetail snatch);
}
