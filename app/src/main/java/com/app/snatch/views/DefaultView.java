package com.app.snatch.views;

/**
 * Created by taufik on 11/27/17.
 */

public interface DefaultView extends BaseView {
    
    void onDefaultFail(String message);
    
    void onDefaultSuccess(String message);
}
