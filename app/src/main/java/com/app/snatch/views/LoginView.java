package com.app.snatch.views;


import com.app.snatch.model.User;

/**
 * Created by Taufik Akbar on 02/03/2017.
 */

public interface LoginView extends BaseView {
    
    void onLoginFail(String message);

    void onLoginSuccess(User user);
}
