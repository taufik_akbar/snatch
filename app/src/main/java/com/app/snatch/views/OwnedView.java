package com.app.snatch.views;


import com.app.snatch.model.Snatch;

import java.util.ArrayList;

/**
 * Created by Taufik Akbar on 02/03/2017.
 */

public interface OwnedView extends BaseView {
    
    void onOwnedFail(String message);

    void onOwnedSuccess(ArrayList<Snatch> snatches);
}
