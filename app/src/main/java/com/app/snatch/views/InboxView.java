package com.app.snatch.views;


import com.app.snatch.model.Message;

import java.util.ArrayList;

/**
 * Created by Taufik Akbar on 02/03/2017.
 */

public interface InboxView extends BaseView {
    
    void onInboxFail(String message);

    void onInboxSuccess(ArrayList<Message> messages);
}
