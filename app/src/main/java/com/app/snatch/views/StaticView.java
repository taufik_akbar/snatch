package com.app.snatch.views;


import com.app.snatch.model.response.StaticResponse;

/**
 * Created by Taufik Akbar on 02/03/2017.
 */

public interface StaticView extends BaseView {
    
    void onStaticFail(String message);

    void onStaticSuccess(StaticResponse user);
}
