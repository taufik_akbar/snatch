package com.app.snatch.presenter;


import com.app.snatch.api.ApiService;
import com.app.snatch.injection.BaseApp;
import com.app.snatch.model.response.StaticResponse;
import com.app.snatch.utils.Constants;
import com.app.snatch.utils.ErrorHelper;
import com.app.snatch.views.StaticView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Taufik Akbar on 14/03/2017.
 */

public class StaticPresenter implements BasePresenter<StaticView>, Constants {

    @Inject
    ApiService apiService;

    private StaticView staticView;
    private StaticResponse response;
    private CompositeDisposable compositeDisposable;

    public StaticPresenter() {
        BaseApp.getAppComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(StaticView view) {
        this.staticView = view;
    }

    @Override
    public void detachView() {
        this.staticView = null;
        compositeDisposable.clear();
    }

    public void getStatic(String token) {
        staticView.showProgressDialog();

        compositeDisposable.add(apiService.getStatic(token)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<StaticResponse>() {
                    @Override
                    public void onNext(StaticResponse response) {
                        StaticPresenter.this.response = response;

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        try {
                            staticView.onStaticFail(ErrorHelper.rxError(e));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        onNextAction(response);
                    }
                }));
    }

    private void onNextAction(StaticResponse response) {
        
        if (response.getMessage().equalsIgnoreCase("ok")) {
            staticView.onStaticSuccess(response);
        } else {
            staticView.onStaticFail(response.getMessage());
        }
    }
}
