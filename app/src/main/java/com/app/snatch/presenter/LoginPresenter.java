package com.app.snatch.presenter;


import com.app.snatch.api.ApiService;
import com.app.snatch.injection.BaseApp;
import com.app.snatch.model.request.LoginRequest;
import com.app.snatch.model.response.UserAccessResponse;
import com.app.snatch.utils.Constants;
import com.app.snatch.utils.ErrorHelper;
import com.app.snatch.views.LoginView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Taufik Akbar on 14/03/2017.
 */

public class LoginPresenter implements BasePresenter<LoginView>, Constants {

    @Inject
    ApiService apiService;

    private LoginView loginView;
    private UserAccessResponse response;
    private CompositeDisposable compositeDisposable;

    public LoginPresenter() {
        BaseApp.getAppComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(LoginView view) {
        this.loginView = view;
    }

    @Override
    public void detachView() {
        this.loginView = null;
        compositeDisposable.clear();
    }

    public void postLogin(LoginRequest loginRequest) {
        loginView.showProgressDialog();

        compositeDisposable.add(apiService.postLogin(loginRequest)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<UserAccessResponse>() {
                    @Override
                    public void onNext(UserAccessResponse response) {
                        LoginPresenter.this.response = response;

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        try {
                            loginView.onLoginFail(ErrorHelper.rxError(e));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        onNextAction(response);
                    }
                }));
    }

    private void onNextAction(UserAccessResponse response) {
        
        if (response.getMessage().equalsIgnoreCase("ok")) {
            loginView.onLoginSuccess(response.getUserAccess());
        } else {
            loginView.onLoginFail(response.getMessage());
        }
    }
}
