package com.app.snatch.presenter;


import com.app.snatch.api.ApiService;
import com.app.snatch.injection.BaseApp;
import com.app.snatch.model.response.OwnedResponse;
import com.app.snatch.utils.Constants;
import com.app.snatch.utils.ErrorHelper;
import com.app.snatch.views.OwnedView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Taufik Akbar on 14/03/2017.
 */

public class SnatchListPresenter implements BasePresenter<OwnedView>, Constants {

    @Inject
    ApiService apiService;

    private OwnedView ownedView;
    private OwnedResponse response;
    private CompositeDisposable compositeDisposable;

    public SnatchListPresenter() {
        BaseApp.getAppComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(OwnedView view) {
        this.ownedView = view;
    }

    @Override
    public void detachView() {
        this.ownedView = null;
        compositeDisposable.clear();
    }

    public void getCategoryList(String token, String type, String page, String limit) {
        ownedView.showProgressDialog();

        compositeDisposable.add(apiService.getCategoryDataList(token, type, page, limit)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<OwnedResponse>() {
                    @Override
                    public void onNext(OwnedResponse response) {
                        SnatchListPresenter.this.response = response;

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        try {
                            ownedView.onOwnedFail(ErrorHelper.rxError(e));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        onNextAction(response);
                    }
                }));
    }
    

    private void onNextAction(OwnedResponse response) {
        
        if (response.getMessage().equalsIgnoreCase("ok")) {
            ownedView.onOwnedSuccess(response.getSnatch().getSnatches());
        } else {
            ownedView.onOwnedFail(response.getMessage());
        }
    }
}
