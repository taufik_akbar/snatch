package com.app.snatch.presenter;


import com.app.snatch.api.ApiService;
import com.app.snatch.model.AddSnatchRequest;
import com.app.snatch.model.response.DefaultResponse;
import com.app.snatch.utils.Constants;
import com.app.snatch.utils.ErrorHelper;
import com.app.snatch.views.DefaultView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by Taufik Akbar on 14/03/2017.
 */

public class GetLocationPresenter implements BasePresenter<DefaultView>, Constants {

    @Inject
    ApiService apiService;
    @Inject
    Retrofit retrofit;

    private DefaultView defaultView;
    private DefaultResponse response;
    private CompositeDisposable compositeDisposable;

    public GetLocationPresenter() {
        
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(DefaultView view) {
        this.defaultView = view;
    }

    @Override
    public void detachView() {
        this.defaultView = null;
        compositeDisposable.clear();
    }

    public void addSnatch(String token, AddSnatchRequest requestBody) {
        defaultView.showProgressDialog();

        compositeDisposable.add(apiService.addSnatch(token, requestBody)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<DefaultResponse>() {
                    @Override
                    public void onNext(DefaultResponse response) {
                        GetLocationPresenter.this.response = response;

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        try {
                            defaultView.onDefaultFail(ErrorHelper.rxError(e));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        onNextAction(response);
                    }
                }));
    }

    private void onNextAction(DefaultResponse response) {
        
        if (response.getMessage().equalsIgnoreCase("ok")) {
            defaultView.onDefaultSuccess(response.getMessage());
        } else {
            defaultView.onDefaultFail(response.getMessage());
        }
    }
}
