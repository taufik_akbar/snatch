package com.app.snatch.presenter;


import com.app.snatch.api.ApiService;
import com.app.snatch.injection.BaseApp;
import com.app.snatch.model.response.InboxResponse;
import com.app.snatch.utils.Constants;
import com.app.snatch.utils.ErrorHelper;
import com.app.snatch.views.InboxView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Taufik Akbar on 14/03/2017.
 */

public class InboxPresenter implements BasePresenter<InboxView>, Constants {

    @Inject
    ApiService apiService;

    private InboxView inboxView;
    private InboxResponse response;
    private CompositeDisposable compositeDisposable;

    public InboxPresenter() {
        BaseApp.getAppComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(InboxView view) {
        this.inboxView = view;
    }

    @Override
    public void detachView() {
        this.inboxView = null;
        compositeDisposable.clear();
    }

    public void getInbox(String token) {
        inboxView.showProgressDialog();

        compositeDisposable.add(apiService.getInbox(token)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<InboxResponse>() {
                    @Override
                    public void onNext(InboxResponse response) {
                        InboxPresenter.this.response = response;

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        try {
                            inboxView.onInboxFail(ErrorHelper.rxError(e));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        onNextAction(response);
                    }
                }));
    }
    

    private void onNextAction(InboxResponse response) {
        
        if (response.getMessage().equalsIgnoreCase("ok")) {
            inboxView.onInboxSuccess(response.getInboxMessage());
        } else {
            inboxView.onInboxFail(response.getMessage());
        }
    }
}
