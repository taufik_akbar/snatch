package com.app.snatch.presenter;


import com.app.snatch.api.ApiService;
import com.app.snatch.injection.BaseApp;
import com.app.snatch.model.response.ProfileResponse;
import com.app.snatch.utils.Constants;
import com.app.snatch.utils.ErrorHelper;
import com.app.snatch.views.ProfileView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Taufik Akbar on 14/03/2017.
 */

public class ProfilePresenter implements BasePresenter<ProfileView>, Constants {

    @Inject
    ApiService apiService;

    private ProfileView profileView;
    private ProfileResponse response;
    private CompositeDisposable compositeDisposable;

    public ProfilePresenter() {
        BaseApp.getAppComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(ProfileView view) {
        this.profileView = view;
    }

    @Override
    public void detachView() {
        this.profileView = null;
        compositeDisposable.clear();
    }

    public void getProfile(String token) {
        profileView.showProgressDialog();

        compositeDisposable.add(apiService.getProfile(token)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ProfileResponse>() {
                    @Override
                    public void onNext(ProfileResponse response) {
                        ProfilePresenter.this.response = response;

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        try {
                            profileView.onProfileFail(ErrorHelper.rxError(e));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        onNextAction(response);
                    }
                }));
    }

    private void onNextAction(ProfileResponse response) {
        
        if (response.getMessage().equalsIgnoreCase("ok")) {
            profileView.onProfileSuccess(response.getUser());
        } else {
            profileView.onProfileFail(response.getMessage());
        }
    }
}
