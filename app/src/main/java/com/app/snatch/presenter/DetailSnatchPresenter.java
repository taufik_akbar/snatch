package com.app.snatch.presenter;


import com.app.snatch.api.ApiService;
import com.app.snatch.injection.BaseApp;
import com.app.snatch.model.response.SnatchDetailResponse;
import com.app.snatch.utils.Constants;
import com.app.snatch.utils.ErrorHelper;
import com.app.snatch.views.DetailSnatchView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Taufik Akbar on 14/03/2017.
 */

public class DetailSnatchPresenter implements BasePresenter<DetailSnatchView>, Constants {

    @Inject
    ApiService apiService;

    private DetailSnatchView detailSnatchView;
    private SnatchDetailResponse response;
    private CompositeDisposable compositeDisposable;

    public DetailSnatchPresenter() {
        BaseApp.getAppComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(DetailSnatchView view) {
        this.detailSnatchView = view;
    }

    @Override
    public void detachView() {
        this.detailSnatchView = null;
        compositeDisposable.clear();
    }

    public void getDetail(String token, String snatchId) {
        detailSnatchView.showProgressDialog();

        compositeDisposable.add(apiService.getSnatchDetail(token, snatchId)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<SnatchDetailResponse>() {
                    @Override
                    public void onNext(SnatchDetailResponse response) {
                        DetailSnatchPresenter.this.response = response;

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        try {
                            detailSnatchView.onDetailFail(ErrorHelper.rxError(e));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        onNextAction(response);
                    }
                }));
    }
    

    private void onNextAction(SnatchDetailResponse response) {
        
        if (response.getMessage().equalsIgnoreCase("ok")) {
            detailSnatchView.onDetailSuccess(response.getSnatchDetail());
        } else {
            detailSnatchView.onDetailFail(response.getMessage());
        }
    }
}
