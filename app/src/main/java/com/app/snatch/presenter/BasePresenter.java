package com.app.snatch.presenter;

/**
 * Created by taufik on 11/27/17.
 */

public interface BasePresenter<V> {
    
    void attachView(V view);
    
    void detachView();
}