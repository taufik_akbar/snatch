package com.app.snatch.presenter;


import com.app.snatch.api.ApiService;
import com.app.snatch.injection.BaseApp;
import com.app.snatch.model.response.DefaultResponse;
import com.app.snatch.utils.Constants;
import com.app.snatch.utils.ErrorHelper;
import com.app.snatch.views.DefaultView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;

/**
 * Created by Taufik Akbar on 14/03/2017.
 */

public class EditProfilePresenter implements BasePresenter<DefaultView>, Constants {

    @Inject
    ApiService apiService;

    private DefaultView defaultView;
    private DefaultResponse response;
    private CompositeDisposable compositeDisposable;

    public EditProfilePresenter() {
        BaseApp.getAppComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(DefaultView view) {
        this.defaultView = view;
    }

    @Override
    public void detachView() {
        this.defaultView = null;
        compositeDisposable.clear();
    }

    public void editProfile(String token, RequestBody requestBody) {
        defaultView.showProgressDialog();

        compositeDisposable.add(apiService.createSnatch(token, requestBody)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<DefaultResponse>() {
                    @Override
                    public void onNext(DefaultResponse response) {
                        EditProfilePresenter.this.response = response;

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        try {
                            defaultView.onDefaultFail(ErrorHelper.rxError(e));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        onNextAction(response);
                    }
                }));
    }

    private void onNextAction(DefaultResponse response) {
        
        if (response.getMessage().equalsIgnoreCase("ok")) {
            defaultView.onDefaultSuccess(response.getMessage());
        } else {
            defaultView.onDefaultFail(response.getMessage());
        }
    }
}
