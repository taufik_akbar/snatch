package com.app.snatch.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taufik on 11/28/17.
 */

public class LoginRequest {
    
    @SerializedName("user_email")
    private String email;
    @SerializedName("user_password")
    private String pass;
    @SerializedName("fcm_token")
    private String fcmToken;
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPass() {
        return pass;
    }
    
    public void setPass(String pass) {
        this.pass = pass;
    }
    
    public String getFcmToken() {
        return fcmToken;
    }
    
    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
