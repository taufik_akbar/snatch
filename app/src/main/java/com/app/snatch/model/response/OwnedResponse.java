package com.app.snatch.model.response;

import com.app.snatch.model.Snatch;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by taufik on 11/28/17.
 */

public class OwnedResponse extends DefaultResponse {

    @SerializedName("data")
    private SnatchData snatch;
    
    public SnatchData getSnatch() {
        return snatch;
    }
    
    public class SnatchData {
        
        @SerializedName("list")
        private ArrayList<Snatch> snatches;
    
        public ArrayList<Snatch> getSnatches() {
            return snatches;
        }
    }
}
