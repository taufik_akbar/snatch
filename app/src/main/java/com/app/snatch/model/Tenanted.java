package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 1/22/18.
 */

public class Tenanted implements Serializable {
    
    @SerializedName("tenanted_id")
    private String tenantedId;
    @SerializedName("tenanted_value")
    private String tenanted;
    
    public String getTenantedId() {
        return tenantedId;
    }
    
    public void setTenantedId(String tenantedId) {
        this.tenantedId = tenantedId;
    }
    
    public String getTenanted() {
        return tenanted;
    }
    
    public void setTenanted(String tenanted) {
        this.tenanted = tenanted;
    }
}
