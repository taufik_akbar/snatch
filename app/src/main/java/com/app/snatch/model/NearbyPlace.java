package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/22/17.
 */

public class NearbyPlace implements Serializable {
    
    @SerializedName("nearby_place_id")
    private String nearbyPlaceId;
    @SerializedName("nearby_place_value")
    private String nearbyPlace;
    private boolean isSelected;
    
    public String getNearbyPlaceId() {
        return nearbyPlaceId;
    }
    
    public void setNearbyPlaceId(String nearbyPlaceId) {
        this.nearbyPlaceId = nearbyPlaceId;
    }
    
    public String getNearbyPlace() {
        return nearbyPlace;
    }
    
    public void setNearbyPlace(String nearbyPlace) {
        this.nearbyPlace = nearbyPlace;
    }
    
    public boolean isSelected() {
        return isSelected;
    }
    
    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
