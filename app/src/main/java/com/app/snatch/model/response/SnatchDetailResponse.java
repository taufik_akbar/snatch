package com.app.snatch.model.response;

import com.app.snatch.model.SnatchDetail;
import com.google.gson.annotations.SerializedName;

/**
 * Created by taufik on 11/28/17.
 */

public class SnatchDetailResponse extends DefaultResponse {

    @SerializedName("data")
    private SnatchDetail snatchDetail;
    
    public SnatchDetail getSnatchDetail() {
        return snatchDetail;
    }
}
