package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 1/6/18.
 */

public class StaticDetail implements Serializable {
    
    @SerializedName("id")
    private String id;
    @SerializedName("value")
    private String value;
    private int image;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public int getImage() {
        return image;
    }
    
    public void setImage(int image) {
        this.image = image;
    }
}
