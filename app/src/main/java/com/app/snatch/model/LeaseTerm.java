package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 1/22/18.
 */

public class LeaseTerm implements Serializable {
    
    @SerializedName("lease_term_id")
    private String leaseTermId;
    @SerializedName("lease_term_value")
    private String leaseTerm;
    
    public String getLeaseTermId() {
        return leaseTermId;
    }
    
    public void setLeaseTermId(String leaseTermId) {
        this.leaseTermId = leaseTermId;
    }
    
    public String getLeaseTerm() {
        return leaseTerm;
    }
    
    public void setLeaseTerm(String leaseTerm) {
        this.leaseTerm = leaseTerm;
    }
}
