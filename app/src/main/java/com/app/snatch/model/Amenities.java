package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/22/17.
 */

public class Amenities implements Serializable {
    
    @SerializedName("amenity_id")
    private String amenitiesId;
    @SerializedName("amenity_value")
    private String amenitiesName;
    private boolean isSelected;
    
    public String getAmenitiesId() {
        return amenitiesId;
    }
    
    public void setAmenitiesId(String amenitiesId) {
        this.amenitiesId = amenitiesId;
    }
    
    public String getAmenitiesName() {
        return amenitiesName;
    }
    
    public void setAmenitiesName(String amenitiesName) {
        this.amenitiesName = amenitiesName;
    }
    
    public boolean isSelected() {
        return isSelected;
    }
    
    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
