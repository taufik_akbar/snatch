package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 11/28/17.
 */

public class User extends UserAccess implements Serializable {
 
    @SerializedName("user_id")
    private String userId;
    @SerializedName("user_name")
    private String name;
    @SerializedName("user_cea")
    private String cea;
    @SerializedName("user_designation")
    private String designation;
    @SerializedName("user_image")
    private ImagePath imagePath;
    @SerializedName("user_gender")
    private String gender;
    
    public ImagePath getImagePath() {
        return imagePath;
    }
    
    public String getUserId() {
        return userId;
    }
    
    public String getName() {
        return name;
    }
    
    public String getCea() {
        return cea;
    }
    
    public String getDesignation() {
        return designation;
    }
    
    public String getGender() {
        return gender;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setCea(String cea) {
        this.cea = cea;
    }
    
    public void setDesignation(String designation) {
        this.designation = designation;
    }
    
    public void setImagePath(ImagePath imagePath) {
        this.imagePath = imagePath;
    }
    
    public void setGender(String gender) {
        this.gender = gender;
    }
}
