package com.app.snatch.model.response;

import com.app.snatch.model.User;
import com.google.gson.annotations.SerializedName;

/**
 * Created by taufik on 11/28/17.
 */

public class UserAccessResponse extends DefaultResponse {

    @SerializedName("data")
    private User userAccess;
    
    public User getUserAccess() {
        return userAccess;
    }
    
    public void setUserAccess(User userAccess) {
        this.userAccess = userAccess;
    }
}
