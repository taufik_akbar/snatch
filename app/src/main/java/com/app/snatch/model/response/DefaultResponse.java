package com.app.snatch.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taufik on 11/28/17.
 */

public class DefaultResponse {
    
    @SerializedName("message")
    String message;
    
    public String getMessage() {
        return message;
    }
}
