package com.app.snatch.model.bus;

/**
 * Created by taufik on 9/5/17.
 */

public class LocationEvent {
    
    private double currentLatitude;
    private double currentLongitude;
    private double currentAltitude;
    
    public LocationEvent(double currentLatitude, double currentLongitude, double currentAltitude) {
        setCurrentLatitude(currentLatitude);
        setCurrentLongitude(currentLongitude);
        setCurrentAltitude(currentAltitude);
    }
    
    public double getCurrentLatitude() {
        return currentLatitude;
    }
    
    public void setCurrentLatitude(double currentLatitude) {
        this.currentLatitude = currentLatitude;
    }
    
    public double getCurrentLongitude() {
        return currentLongitude;
    }
    
    public void setCurrentLongitude(double currentLongitude) {
        this.currentLongitude = currentLongitude;
    }
    
    public double getCurrentAltitude() {
        return currentAltitude;
    }
    
    public void setCurrentAltitude(double currentAltitude) {
        this.currentAltitude = currentAltitude;
    }
}
