package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/18/17.
 */

public class PriceType implements Serializable {
    
    @SerializedName("id")
    private String priceId;
    @SerializedName("value")
    private String priceType;
    
    public String getPriceId() {
        return priceId;
    }
    
    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }
    
    public String getPriceType() {
        return priceType;
    }
    
    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }
}
