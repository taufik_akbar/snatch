package com.app.snatch.model.bus;

/**
 * Created by taufik on 1/14/18.
 */

public class SearchAddressEvent {
    
    private String address;
    private String postalCode;
    
    public SearchAddressEvent(String postalCode, String address) {
        setAddress(address);
        setPostalCode(postalCode);
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getPostalCode() {
        return postalCode;
    }
    
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
