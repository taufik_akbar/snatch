package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taufik on 1/8/18.
 */

public class AddSnatchRequest {
    
    @SerializedName("snatch_id")
    private String snatchId;
    
    public String getSnatchId() {
        return snatchId;
    }
    
    public void setSnatchId(String snatchId) {
        this.snatchId = snatchId;
    }
}
