package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 1/22/18.
 */

public class Bedroom implements Serializable {
    
    @SerializedName("bedroom_id")
    private String bedroomId;
    @SerializedName("bedroom_value")
    private String bedroom;
    
    public String getBedroomId() {
        return bedroomId;
    }
    
    public void setBedroomId(String bedroomId) {
        this.bedroomId = bedroomId;
    }
    
    public String getBedroom() {
        return bedroom;
    }
    
    public void setBedroom(String bedroom) {
        this.bedroom = bedroom;
    }
}
