package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taufik on 12/19/17.
 */

public class Message extends Snatch {
    
    @SerializedName("message_id")
    private String messageId;
    @SerializedName("message_text")
    private String message;
    @SerializedName("created")
    private String created;
    
    public String getMessageId() {
        return messageId;
    }
    
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getCreated() {
        return created;
    }
    
    public void setCreated(String created) {
        this.created = created;
    }
}
