package com.app.snatch.model.bus;

import com.app.snatch.model.response.StaticResponse;

/**
 * Created by taufik on 12/18/17.
 */

public class StaticEvent {
    
    private StaticResponse staticResponse;
    
    public StaticEvent(StaticResponse staticResponse) {
        setStaticResponse(staticResponse);
    }
    
    public StaticResponse getStaticResponse() {
        return staticResponse;
    }
    
    public void setStaticResponse(StaticResponse staticResponse) {
        this.staticResponse = staticResponse;
    }
}
