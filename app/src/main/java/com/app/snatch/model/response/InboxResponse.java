package com.app.snatch.model.response;

import com.app.snatch.model.Message;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by taufik on 11/28/17.
 */

public class InboxResponse extends DefaultResponse {
    
    @SerializedName("data")
    private ArrayList<Message> inboxMessage;
    
    public ArrayList<Message> getInboxMessage() {
        return inboxMessage;
    }
    
    public void setInboxMessage(ArrayList<Message> inboxMessage) {
        this.inboxMessage = inboxMessage;
    }
}
