package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/18/17.
 */

public class Tenure implements Serializable {
    
    @SerializedName("tenure_id")
    private String tenureId;
    @SerializedName("tenure_value")
    private String tenure;
    
    public String getTenureId() {
        return tenureId;
    }
    
    public void setTenureId(String tenureId) {
        this.tenureId = tenureId;
    }
    
    public String getTenure() {
        return tenure;
    }
    
    public void setTenure(String tenure) {
        this.tenure = tenure;
    }
}
