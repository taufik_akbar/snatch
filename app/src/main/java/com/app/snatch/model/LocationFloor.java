package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/18/17.
 */

public class LocationFloor implements Serializable {
    
    @SerializedName("id")
    private String floorId;
    @SerializedName("value")
    private String floor;
    
    public String getFloorId() {
        return floorId;
    }
    
    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }
    
    public String getFloor() {
        return floor;
    }
    
    public void setFloor(String floor) {
        this.floor = floor;
    }
}
