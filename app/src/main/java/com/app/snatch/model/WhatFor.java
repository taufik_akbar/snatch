package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 1/12/18.
 */

public class WhatFor implements Serializable {
    
    @SerializedName("what_for_value")
    private String whatFor;
    @SerializedName("what_for_id")
    private String whatForId;
    private boolean isSelected;
    
    public String getWhatFor() {
        return whatFor;
    }
    
    public void setWhatFor(String whatFor) {
        this.whatFor = whatFor;
    }
    
    public String getWhatForId() {
        return whatForId;
    }
    
    public void setWhatForId(String whatForId) {
        this.whatForId = whatForId;
    }
    
    public boolean isSelected() {
        return isSelected;
    }
    
    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
