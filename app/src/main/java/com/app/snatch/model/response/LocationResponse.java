package com.app.snatch.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by taufik on 1/14/18.
 */

public class LocationResponse {
    
    @SerializedName("status")
    private String status;
    @SerializedName("results")
    private ArrayList<Data> dataArrayList;
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public ArrayList<Data> getDataArrayList() {
        return dataArrayList;
    }
    
    public void setDataArrayList(ArrayList<Data> dataArrayList) {
        this.dataArrayList = dataArrayList;
    }
    
    public class Data {
        
        @SerializedName("address_components")
        private ArrayList<AddressComponent> addressComponents;
        @SerializedName("formatted_address")
        private String formattedAddress;
    
        public ArrayList<AddressComponent> getAddressComponents() {
            return addressComponents;
        }
    
        public void setAddressComponents(ArrayList<AddressComponent> addressComponents) {
            this.addressComponents = addressComponents;
        }
    
        public String getFormattedAddress() {
            return formattedAddress;
        }
    
        public void setFormattedAddress(String formattedAddress) {
            this.formattedAddress = formattedAddress;
        }
    }
    
    public class AddressComponent {
    
        @SerializedName("long_name")
        private String longName;
        @SerializedName("short_name")
        private String shortName;
    
        public String getLongName() {
            return longName;
        }
    
        public void setLongName(String longName) {
            this.longName = longName;
        }
    
        public String getShortName() {
            return shortName;
        }
    
        public void setShortName(String shortName) {
            this.shortName = shortName;
        }
    }
}
