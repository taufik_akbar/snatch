package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by taufik on 1/6/18.
 */

public class SnatchDetail extends Snatch {


    @SerializedName("snatch_bedroom")
    private String bedroom;
    @SerializedName("snatch_description")
    private String description;
    @SerializedName("snatch_floor_area")
    private String floorArea;
    @SerializedName("snatch_location_block")
    private String locationBlock;
    @SerializedName("snatch_location_building")
    private String locationBuilding;
    @SerializedName("snatch_location_postal")
    private String locationPostal;
    @SerializedName("snatch_location_road")
    private String locationRoad;
    @SerializedName("snatch_location_lat")
    private String latitude;
    @SerializedName("snatch_location_long")
    private String longitude;
    @SerializedName("snatch_location_x")
    private String locationX;
    @SerializedName("snatch_location_y")
    private String locationY;
    @SerializedName("available_id")
    private String availableId;
    @SerializedName("location_floor_id")
    private String locationFloorId;
    @SerializedName("price_type_id")
    private String priceTypeId;
//    @SerializedName("sub_type_id")
//    private String subTypeId;
    @SerializedName("created")
    private String created;
    @SerializedName("updated")
    private String updated;
    @SerializedName("snatch_amenities")
    private ArrayList<StaticDetail> amenities;
    @SerializedName("snatch_facilities")
    private ArrayList<StaticDetail> facilities;
    @SerializedName("snatcher")
    private ArrayList<User> snatcherList;
    @SerializedName("snatch_images")
    private ArrayList<ImagePath> snatchImage;
    @SerializedName("snatch_measurement")
    private String measurement;
    @SerializedName("snatch_property_group_type")
    private String propertyGroup;
    @SerializedName("snatch_tenure")
    private String tenure;
    @SerializedName("snatched")
    private boolean snatched;
    
    public String getBedroom() {
        return bedroom;
    }
    
    public void setBedroom(String bedroom) {
        this.bedroom = bedroom;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getFloorArea() {
        return floorArea;
    }
    
    public void setFloorArea(String floorArea) {
        this.floorArea = floorArea;
    }
    
    public String getLocationBlock() {
        return locationBlock;
    }
    
    public void setLocationBlock(String locationBlock) {
        this.locationBlock = locationBlock;
    }
    
    public String getLocationBuilding() {
        return locationBuilding;
    }
    
    public void setLocationBuilding(String locationBuilding) {
        this.locationBuilding = locationBuilding;
    }
    
    public String getLocationPostal() {
        return locationPostal;
    }
    
    public void setLocationPostal(String locationPostal) {
        this.locationPostal = locationPostal;
    }
    
    public String getLocationRoad() {
        return locationRoad;
    }
    
    public void setLocationRoad(String locationRoad) {
        this.locationRoad = locationRoad;
    }
    
    public String getLatitude() {
        return latitude;
    }
    
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    
    public String getLongitude() {
        return longitude;
    }
    
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
    public String getLocationX() {
        return locationX;
    }
    
    public void setLocationX(String locationX) {
        this.locationX = locationX;
    }
    
    public String getLocationY() {
        return locationY;
    }
    
    public void setLocationY(String locationY) {
        this.locationY = locationY;
    }
    
    public String getAvailableId() {
        return availableId;
    }
    
    public void setAvailableId(String availableId) {
        this.availableId = availableId;
    }
    
    public String getLocationFloorId() {
        return locationFloorId;
    }
    
    public void setLocationFloorId(String locationFloorId) {
        this.locationFloorId = locationFloorId;
    }
    
    public String getPriceTypeId() {
        return priceTypeId;
    }
    
    public void setPriceTypeId(String priceTypeId) {
        this.priceTypeId = priceTypeId;
    }
    
//    public String getSubTypeId() {
//        return subTypeId;
//    }
//
//    public void setSubTypeId(String subTypeId) {
//        this.subTypeId = subTypeId;
//    }
    
    public String getCreated() {
        return created;
    }
    
    public void setCreated(String created) {
        this.created = created;
    }
    
    public String getUpdated() {
        return updated;
    }
    
    public void setUpdated(String updated) {
        this.updated = updated;
    }
    
    public ArrayList<StaticDetail> getAmenities() {
        return amenities;
    }
    
    public void setAmenities(ArrayList<StaticDetail> amenities) {
        this.amenities = amenities;
    }
    
    public ArrayList<StaticDetail> getFacilities() {
        return facilities;
    }
    
    public void setFacilities(ArrayList<StaticDetail> facilities) {
        this.facilities = facilities;
    }
    
    public ArrayList<User> getSnatcherList() {
        return snatcherList;
    }
    
    public void setSnatcherList(ArrayList<User> snatcherList) {
        this.snatcherList = snatcherList;
    }
    
    public ArrayList<ImagePath> getSnatchImage() {
        return snatchImage;
    }
    
    public void setSnatchImage(ArrayList<ImagePath> snatchImage) {
        this.snatchImage = snatchImage;
    }
    
    public String getMeasurement() {
        return measurement;
    }
    
    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }
    
    public String getPropertyGroup() {
        return propertyGroup;
    }
    
    public void setPropertyGroup(String propertyGroup) {
        this.propertyGroup = propertyGroup;
    }
    
    public String getTenure() {
        return tenure;
    }
    
    public void setTenure(String tenure) {
        this.tenure = tenure;
    }
    
    public boolean isSnatched() {
        return snatched;
    }
    
    public void setSnatched(boolean snatched) {
        this.snatched = snatched;
    }
}
