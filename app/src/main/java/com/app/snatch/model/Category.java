package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by taufik on 12/27/17.
 */

public class Category implements Serializable {
    
    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("category_value")
    private String categoryName;
    private boolean isSelected;
    private ArrayList<Snatch> categorySnatch;
    
    public String getCategoryId() {
        return categoryId;
    }
    
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
    
    public String getCategoryName() {
        return categoryName;
    }
    
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
    public ArrayList<Snatch> getCategorySnatch() {
        return categorySnatch;
    }
    
    public void setCategorySnatch(ArrayList<Snatch> categorySnatch) {
        this.categorySnatch = categorySnatch;
    }
    
    public boolean isSelected() {
        return isSelected;
    }
    
    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
