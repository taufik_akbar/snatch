package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 11/29/17.
 */

public class ImagePath implements Serializable {
    
    @SerializedName("image_path")
    private String image;
    
    public String getImage() {
        return image;
    }
    
    public void setImage(String image) {
        this.image = image;
    }
}
