package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/18/17.
 */

public class Gender implements Serializable {

    @SerializedName("id")
    private String genderId;
    @SerializedName("value")
    private String gender;
    
    public String getGenderId() {
        return genderId;
    }
    
    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }
    
    public String getGender() {
        return gender;
    }
    
    public void setGender(String gender) {
        this.gender = gender;
    }
}
