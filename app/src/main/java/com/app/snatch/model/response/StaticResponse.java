package com.app.snatch.model.response;

import com.app.snatch.model.Amenities;
import com.app.snatch.model.Bedroom;
import com.app.snatch.model.District;
import com.app.snatch.model.LeaseTerm;
import com.app.snatch.model.NearbyPlace;
import com.app.snatch.model.Tenanted;
import com.app.snatch.model.UnitDetail;
import com.app.snatch.model.WhatFor;
import com.app.snatch.model.Category;
import com.app.snatch.model.Gender;
import com.app.snatch.model.LocationFloor;
import com.app.snatch.model.Measurement;
import com.app.snatch.model.PriceType;
import com.app.snatch.model.PropertyType;
import com.app.snatch.model.SubType;
import com.app.snatch.model.Tenure;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by taufik on 12/18/17.
 */

public class StaticResponse extends DefaultResponse implements Serializable {
    
    @SerializedName("data")
    private StaticData staticData;
    
    public StaticData getStaticData() {
        return staticData;
    }
    
    public class StaticData implements Serializable {
        
        
        @SerializedName("gender")
        private ArrayList<Gender> gender;
        @SerializedName("group_type_1")
        private ArrayList<PropertyType> propertyType1;
        @SerializedName("group_type_2")
        private ArrayList<PropertyType> propertyType2;
        @SerializedName("sub_type_1")
        private ArrayList<SubType> subType;
        @SerializedName("sub_type_2")
        private ArrayList<SubType> subType2;
        @SerializedName("snatch_price_type")
        private ArrayList<PriceType> priceType;
        @SerializedName("measurement")
        private ArrayList<Measurement> measurement;
        @SerializedName("snatch_location_floor")
        private ArrayList<LocationFloor> locationFloors;
        @SerializedName("category")
        private ArrayList<Category> categories;
        @SerializedName("facility")
        private ArrayList<NearbyPlace> facilities;
        @SerializedName("amenity")
        private ArrayList<Amenities> amenities;
        @SerializedName("what_for")
        private ArrayList<WhatFor> whatFors;
        @SerializedName("tenure_1")
        private ArrayList<Tenure> tenure1;
        @SerializedName("tenure_2")
        private ArrayList<Tenure> tenure2;
        @SerializedName("bedroom_1")
        private ArrayList<Bedroom> bedroom1;
        @SerializedName("bedroom_2")
        private ArrayList<Bedroom> bedroom2;
        @SerializedName("lease_term")
        private ArrayList<LeaseTerm> leaseTerm;
        @SerializedName("district")
        private ArrayList<District> districts;
        @SerializedName("tenanted")
        private ArrayList<Tenanted> tenanteds;
        @SerializedName("unit_detail_1")
        private ArrayList<UnitDetail> unitDetails1;
        @SerializedName("unit_detail_2")
        private ArrayList<UnitDetail> unitDetails2;
    
        public ArrayList<Tenanted> getTenanteds() {
            return tenanteds;
        }
    
        public void setTenanteds(ArrayList<Tenanted> tenanteds) {
            this.tenanteds = tenanteds;
        }
    
        public ArrayList<UnitDetail> getUnitDetails1() {
            return unitDetails1;
        }
    
        public void setUnitDetails1(ArrayList<UnitDetail> unitDetails1) {
            this.unitDetails1 = unitDetails1;
        }
    
        public ArrayList<UnitDetail> getUnitDetails2() {
            return unitDetails2;
        }
    
        public void setUnitDetails2(ArrayList<UnitDetail> unitDetails2) {
            this.unitDetails2 = unitDetails2;
        }
    
        public ArrayList<District> getDistricts() {
            return districts;
        }
    
        public void setDistricts(ArrayList<District> districts) {
            this.districts = districts;
        }
    
        public ArrayList<Tenure> getTenure1() {
            return tenure1;
        }
    
        public void setTenure1(ArrayList<Tenure> tenure1) {
            this.tenure1 = tenure1;
        }
    
        public ArrayList<Gender> getGender() {
            return gender;
        }
        
        public void setGender(ArrayList<Gender> gender) {
            this.gender = gender;
        }
    
        public ArrayList<PropertyType> getPropertyType1() {
            return propertyType1;
        }
    
        public void setPropertyType1(ArrayList<PropertyType> propertyType1) {
            this.propertyType1 = propertyType1;
        }
    
        public ArrayList<SubType> getSubType() {
            return subType;
        }
        
        public void setSubType(ArrayList<SubType> subType) {
            this.subType = subType;
        }
        
        public ArrayList<PriceType> getPriceType() {
            return priceType;
        }
        
        public void setPriceType(ArrayList<PriceType> priceType) {
            this.priceType = priceType;
        }
        
        
        public ArrayList<Measurement> getMeasurement() {
            return measurement;
        }
        
        public void setMeasurement(ArrayList<Measurement> measurement) {
            this.measurement = measurement;
        }
    
        public ArrayList<SubType> getSubType2() {
            return subType2;
        }
    
        public void setSubType2(ArrayList<SubType> subType2) {
            this.subType2 = subType2;
        }
    
        public ArrayList<LocationFloor> getLocationFloors() {
            return locationFloors;
        }
        
        public void setLocationFloors(ArrayList<LocationFloor> locationFloors) {
            this.locationFloors = locationFloors;
        }
    
        public ArrayList<NearbyPlace> getFacilities() {
            return facilities;
        }
    
        public ArrayList<Amenities> getAmenities() {
            return amenities;
        }
    
        public ArrayList<Category> getCategories() {
            return categories;
        }
    
        public void setCategories(ArrayList<Category> categories) {
            this.categories = categories;
        }
    
        public void setFacilities(ArrayList<NearbyPlace> facilities) {
            this.facilities = facilities;
        }
    
        public void setAmenities(ArrayList<Amenities> amenities) {
            this.amenities = amenities;
        }
    
        public ArrayList<PropertyType> getPropertyType2() {
            return propertyType2;
        }
    
        public void setPropertyType2(ArrayList<PropertyType> propertyType2) {
            this.propertyType2 = propertyType2;
        }
    
        public ArrayList<WhatFor> getWhatFors() {
            return whatFors;
        }
    
        public void setWhatFors(ArrayList<WhatFor> whatFors) {
            this.whatFors = whatFors;
        }
    
        public ArrayList<Tenure> getTenure2() {
            return tenure2;
        }
    
        public void setTenure2(ArrayList<Tenure> tenure2) {
            this.tenure2 = tenure2;
        }
    
        public ArrayList<Bedroom> getBedroom1() {
            return bedroom1;
        }
    
        public void setBedroom1(ArrayList<Bedroom> bedroom1) {
            this.bedroom1 = bedroom1;
        }
    
        public ArrayList<Bedroom> getBedroom2() {
            return bedroom2;
        }
    
        public void setBedroom2(ArrayList<Bedroom> bedroom2) {
            this.bedroom2 = bedroom2;
        }
    
        public ArrayList<LeaseTerm> getLeaseTerm() {
            return leaseTerm;
        }
    
        public void setLeaseTerm(ArrayList<LeaseTerm> leaseTerm) {
            this.leaseTerm = leaseTerm;
        }
    }
}
