package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/18/17.
 */

public class PropertyType implements Serializable {
    
    @SerializedName("group_type_id")
    private String groupTypeId;
    @SerializedName("group_type_value")
    private String groupType;
    private boolean selected;
    
    public String getGroupTypeId() {
        return groupTypeId;
    }
    
    public void setGroupTypeId(String groupTypeId) {
        this.groupTypeId = groupTypeId;
    }
    
    public String getGroupType() {
        return groupType;
    }
    
    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }
    
    public boolean isSelected() {
        return selected;
    }
    
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
