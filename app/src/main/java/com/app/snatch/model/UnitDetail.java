package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 1/22/18.
 */

public class UnitDetail implements Serializable {
    
    @SerializedName("unit_detail_id")
    private String unitDetailId;
    @SerializedName("unit_detail_value")
    private String unitDetail;
    
    public String getUnitDetailId() {
        return unitDetailId;
    }
    
    public void setUnitDetailId(String unitDetailId) {
        this.unitDetailId = unitDetailId;
    }
    
    public String getUnitDetail() {
        return unitDetail;
    }
    
    public void setUnitDetail(String unitDetail) {
        this.unitDetail = unitDetail;
    }
}
