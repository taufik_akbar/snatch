package com.app.snatch.model.request;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by taufik on 1/13/18.
 */

public class CreateSnatchRequest implements Serializable {
    
    private String whatForId;
    private String whatFor;
    private String snatchTypeId;
    private String subType;
    private String snatchCategory;
    private String snatchPropertyGroupType;
    private String snatchBedroom;
    private String bedroomId;
    private String snatchDescription;
    private String snatchFloorArea;
    private String buildMeasurement;
    private String landMeasurement;
    private String buildMeasurementId;
    private String landMeasurementId;
    private String snatchLocationBuilding;
    private String snatchLocationBlock;
    private String locationRoad;
    private String latitude;
    private String longitude;
    private String locationX;
    private String locationY;
    private String address;
    private String snatchName;
    private String snatchPrice;
    private String imageId;
    private String listTypeId;
    private String locationFloorId;
    private String priceTypeId;
    private String statusId;
    private String subTypeId;
    private String propertyGroupId;
    private String tenureID;
    private String categoryId;
    private String districtId;
    private String bathroom;
    private String postalCode;
    private String leaseTermId;
    private String tenantedId;
    private String unitDetailId;
    private ArrayList<String> arrayImage;
    private ArrayList<String> arrayAmenities;
    private ArrayList<String> arrayNearby;
    
    public String getBathroom() {
        return bathroom;
    }
    
    public void setBathroom(String bathroom) {
        this.bathroom = bathroom;
    }
    
    public String getPropertyGroupId() {
        return propertyGroupId;
    }
    
    public void setPropertyGroupId(String propertyGroupId) {
        this.propertyGroupId = propertyGroupId;
    }
    
    public String getLeaseTermId() {
        return leaseTermId;
    }
    
    public void setLeaseTermId(String leaseTermId) {
        this.leaseTermId = leaseTermId;
    }
    
    public String getTenantedId() {
        return tenantedId;
    }
    
    public void setTenantedId(String tenantedId) {
        this.tenantedId = tenantedId;
    }
    
    public String getUnitDetailId() {
        return unitDetailId;
    }
    
    public void setUnitDetailId(String unitDetailId) {
        this.unitDetailId = unitDetailId;
    }
    
    public String getBuildMeasurement() {
        return buildMeasurement;
    }
    
    public void setBuildMeasurement(String buildMeasurement) {
        this.buildMeasurement = buildMeasurement;
    }
    
    public String getLandMeasurement() {
        return landMeasurement;
    }
    
    public void setLandMeasurement(String landMeasurement) {
        this.landMeasurement = landMeasurement;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getDistrictId() {
        return districtId;
    }
    
    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }
    
    public String getBedroomId() {
        return bedroomId;
    }
    
    public void setBedroomId(String bedroomId) {
        this.bedroomId = bedroomId;
    }
    
    public String getWhatForId() {
        return whatForId;
    }
    
    public void setWhatForId(String whatForId) {
        this.whatForId = whatForId;
    }
    
    public String getSubType() {
        return subType;
    }
    
    public void setSubType(String subType) {
        this.subType = subType;
    }
    
    public String getWhatFor() {
        return whatFor;
    }
    
    public void setWhatFor(String whatFor) {
        this.whatFor = whatFor;
    }
    
    public String getSnatchTypeId() {
        return snatchTypeId;
    }
    
    public void setSnatchTypeId(String snatchTypeId) {
        this.snatchTypeId = snatchTypeId;
    }
    
    public String getSnatchPropertyGroupType() {
        return snatchPropertyGroupType;
    }
    
    public void setSnatchPropertyGroupType(String snatchPropertyGroupType) {
        this.snatchPropertyGroupType = snatchPropertyGroupType;
    }
    
    public String getSnatchCategory() {
        return snatchCategory;
    }
    
    public void setSnatchCategory(String snatchCategory) {
        this.snatchCategory = snatchCategory;
    }
    
    public String getSnatchBedroom() {
        return snatchBedroom;
    }
    
    public void setSnatchBedroom(String snatchBedroom) {
        this.snatchBedroom = snatchBedroom;
    }
    
    public String getSnatchDescription() {
        return snatchDescription;
    }
    
    public void setSnatchDescription(String snatchDescription) {
        this.snatchDescription = snatchDescription;
    }
    
    public String getLandMeasurementId() {
        return landMeasurementId;
    }
    
    public void setLandMeasurementId(String landMeasurementId) {
        this.landMeasurementId = landMeasurementId;
    }
    
    public String getSnatchFloorArea() {
        return snatchFloorArea;
    }
    
    public void setSnatchFloorArea(String snatchFloorArea) {
        this.snatchFloorArea = snatchFloorArea;
    }
    
    public String getBuildMeasurementId() {
        return buildMeasurementId;
    }
    
    public void setBuildMeasurementId(String buildMeasurementId) {
        this.buildMeasurementId = buildMeasurementId;
    }
    
    public String getSnatchLocationBuilding() {
        return snatchLocationBuilding;
    }
    
    public void setSnatchLocationBuilding(String snatchLocationBuilding) {
        this.snatchLocationBuilding = snatchLocationBuilding;
    }
    
    public String getSnatchLocationBlock() {
        return snatchLocationBlock;
    }
    
    public void setSnatchLocationBlock(String snatchLocationBlock) {
        this.snatchLocationBlock = snatchLocationBlock;
    }
    
    public String getPostalCode() {
        return postalCode;
    }
    
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    
    public String getLocationRoad() {
        return locationRoad;
    }
    
    public void setLocationRoad(String locationRoad) {
        this.locationRoad = locationRoad;
    }
    
    public String getLatitude() {
        return latitude;
    }
    
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    
    public String getLongitude() {
        return longitude;
    }
    
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
    public String getLocationX() {
        return locationX;
    }
    
    public void setLocationX(String locationX) {
        this.locationX = locationX;
    }
    
    public String getLocationY() {
        return locationY;
    }
    
    public void setLocationY(String locationY) {
        this.locationY = locationY;
    }
    
    public String getSnatchName() {
        return snatchName;
    }
    
    public void setSnatchName(String snatchName) {
        this.snatchName = snatchName;
    }
    
    public String getSnatchPrice() {
        return snatchPrice;
    }
    
    public void setSnatchPrice(String snatchPrice) {
        this.snatchPrice = snatchPrice;
    }
    
    public String getImageId() {
        return imageId;
    }
    
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
    
    public String getListTypeId() {
        return listTypeId;
    }
    
    public void setListTypeId(String listTypeId) {
        this.listTypeId = listTypeId;
    }
    
    public String getLocationFloorId() {
        return locationFloorId;
    }
    
    public void setLocationFloorId(String locationFloorId) {
        this.locationFloorId = locationFloorId;
    }
    
    public String getCategoryId() {
        return categoryId;
    }
    
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
    
    public String getPriceTypeId() {
        return priceTypeId;
    }
    
    public void setPriceTypeId(String priceTypeId) {
        this.priceTypeId = priceTypeId;
    }
    
    public String getStatusId() {
        return statusId;
    }
    
    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }
    
    public String getSubTypeId() {
        return subTypeId;
    }
    
    public void setSubTypeId(String subTypeId) {
        this.subTypeId = subTypeId;
    }
    
    public String getTenureID() {
        return tenureID;
    }
    
    public void setTenureID(String tenureID) {
        this.tenureID = tenureID;
    }
    
    public ArrayList<String> getArrayImage() {
        return arrayImage;
    }
    
    public void setArrayImage(ArrayList<String> arrayImage) {
        this.arrayImage = arrayImage;
    }
    
    public ArrayList<String> getArrayAmenities() {
        return arrayAmenities;
    }
    
    public void setArrayAmenities(ArrayList<String> arrayAmenities) {
        this.arrayAmenities = arrayAmenities;
    }
    
    public ArrayList<String> getArrayNearby() {
        return arrayNearby;
    }
    
    public void setArrayNearby(ArrayList<String> arrayNearby) {
        this.arrayNearby = arrayNearby;
    }
    
    
}
