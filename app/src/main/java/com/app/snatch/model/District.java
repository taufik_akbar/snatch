package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 1/22/18.
 */

public class District implements Serializable {
    
    @SerializedName("district_id")
    private String districtId;
    @SerializedName("district_value")
    private String district;
    
    public String getDistrictId() {
        return districtId;
    }
    
    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }
    
    public String getDistrict() {
        return district;
    }
    
    public void setDistrict(String district) {
        this.district = district;
    }
}
