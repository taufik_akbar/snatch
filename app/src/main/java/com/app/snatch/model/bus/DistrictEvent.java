package com.app.snatch.model.bus;

/**
 * Created by taufik on 1/14/18.
 */

public class DistrictEvent {
    
    private String district;
    private String districtId;
    
    public DistrictEvent(String district, String districtId) {
        setDistrict(district);
        setDistrictId(districtId);
    }
    
    public String getDistrict() {
        return district;
    }
    
    public String getDistrictId() {
        return districtId;
    }
    
    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }
    
    public void setDistrict(String district) {
        this.district = district;
    }
}
