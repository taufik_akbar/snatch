package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/18/17.
 */

public class Snatch implements Serializable {
    
    @SerializedName("snatch_id")
    private String snatchId;
    @SerializedName("address")
    private String snatchName;
    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("group_type_id")
    private String groupTypeId;
    @SerializedName("sub_type_id")
    private String subTypeId;
    @SerializedName("district_id")
    private String districtId;
    @SerializedName("building_name")
    private String buildingName;
    @SerializedName("snatch_category")
    private String snatchType;
    @SerializedName("price")
    private String snatchPrice;
    @SerializedName("status_id")
    private String statusId;
    @SerializedName("image")
    private ImagePath imagePath;
    @SerializedName("user")
    private User user;
    
    public String getDistrictId() {
        return districtId;
    }
    
    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }
    
    public String getBuildingName() {
        return buildingName;
    }
    
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }
    
    public String getSnatchId() {
        return snatchId;
    }
    
    public String getSnatchName() {
        return snatchName;
    }
    
    public String getSnatchType() {
        return snatchType;
    }
    
    public String getSnatchPrice() {
        return snatchPrice;
    }
    
    public String getCategoryId() {
        return categoryId;
    }
    
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
    
    public String getGroupTypeId() {
        return groupTypeId;
    }
    
    public void setGroupTypeId(String groupTypeId) {
        this.groupTypeId = groupTypeId;
    }
    
    public String getSubTypeId() {
        return subTypeId;
    }
    
    public void setSubTypeId(String subTypeId) {
        this.subTypeId = subTypeId;
    }
    
    public String getStatusId() {
        return statusId;
    }
    
    public ImagePath getImagePath() {
        return imagePath;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setSnatchId(String snatchId) {
        this.snatchId = snatchId;
    }
    
    public void setSnatchName(String snatchName) {
        this.snatchName = snatchName;
    }
    
    public void setSnatchType(String snatchType) {
        this.snatchType = snatchType;
    }
    
    public void setSnatchPrice(String snatchPrice) {
        this.snatchPrice = snatchPrice;
    }
    
    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }
    
    public void setImagePath(ImagePath imagePath) {
        this.imagePath = imagePath;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
}
