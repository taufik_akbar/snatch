package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/18/17.
 */

public class Measurement implements Serializable {
    
    @SerializedName("measurement_id")
    private String measurementId;
    @SerializedName("measurement_value")
    private String measurement;
    
    public String getMeasurementId() {
        return measurementId;
    }
    
    public void setMeasurementId(String measurementId) {
        this.measurementId = measurementId;
    }
    
    public String getMeasurement() {
        return measurement;
    }
    
    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }
}
