package com.app.snatch.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by taufik on 12/18/17.
 */

public class SubType implements Serializable {
    
    @SerializedName("sub_type_id")
    private String subTypeId;
    @SerializedName("sub_type_value")
    private String subType;
    
    public String getSubTypeId() {
        return subTypeId;
    }
    
    public void setSubTypeId(String subTypeId) {
        this.subTypeId = subTypeId;
    }
    
    public String getSubType() {
        return subType;
    }
    
    public void setSubType(String subType) {
        this.subType = subType;
    }
}
