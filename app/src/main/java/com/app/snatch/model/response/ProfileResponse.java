package com.app.snatch.model.response;

import com.app.snatch.model.User;
import com.google.gson.annotations.SerializedName;

/**
 * Created by taufik on 11/28/17.
 */

public class ProfileResponse extends DefaultResponse {

    @SerializedName("data")
    private User user;
    
    public User getUser() {
        return user;
    }
}
