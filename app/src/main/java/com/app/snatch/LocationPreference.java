package com.app.snatch;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.snatch.utils.Constants;

/**
 * Created by taufik on 5/1/17.
 */

public class LocationPreference implements Constants {
    
    private final static String LATITUDE = "LATITUDE";
    private final static String LONGITUDE = "LONGITUDE";

    public static void setLocation(Context context, String lat, String lon) {
        
        SharedPreferences prefs = context.getSharedPreferences(PREF_LOCATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = prefs.edit();
        mEditor.putString(LATITUDE, lat);
        mEditor.putString(LONGITUDE, lon);
        
        mEditor.apply();
    }

    public static String getLatitude(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_LOCATION, Context.MODE_PRIVATE);
        return preferences.getString(LATITUDE, DEFAULT_STRING_0);
    }
    
    public static String getLongitude(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_LOCATION, Context.MODE_PRIVATE);
        return preferences.getString(LONGITUDE, DEFAULT_STRING_0);
    }
    
}
