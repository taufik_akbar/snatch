package com.app.snatch.dialog;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.snatch.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListDialog extends BaseDialogFragment {
    
    
    public ListDialog() {
        // Required empty public constructor
    }
    
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.dialog_list;
    }
    
}
