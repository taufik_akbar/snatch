package com.app.snatch.dialog;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.app.snatch.R;
import com.app.snatch.utils.DividerItemDecoration;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseDialogFragment extends DialogFragment {
    
    protected Unbinder unbinder;
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        int width = getResources().getDimensionPixelSize(R.dimen.dimen324);
        // create ContextThemeWrapper from the original Activity Context with the custom theme
        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme);
        
        // clone the inflater using the ContextThemeWrapper
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            getDialog().getWindow().setLayout(width, 0);
        }
        return localInflater.inflate(getFragmentLayout(), container, false);
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }
    
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (dialog.getWindow() != null)
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        
        return dialog;
    }
    
    protected abstract int getFragmentLayout();
    
    
    
    private void injectViews(final View view) {
        unbinder = ButterKnife.bind(this, view);
    }
    
//    protected void showWarningDialog(String titleWarning, String message, String btnText) {
//        WarningDialog dialog = WarningDialog.newInstance(titleWarning, message, btnText);
//        dialog.show(getActivity().getSupportFragmentManager(), WarningDialog.TAG);
//    }
    protected void loadFragment(int container, Fragment fragment, String tag) {
        FragmentManager manager = getFragmentManager();
        if (manager.findFragmentByTag(tag) == null)
            manager.beginTransaction().add(container, fragment, tag).addToBackStack(tag).commit();
    }
    protected void bindRecycler(RecyclerView mRecycler, int drawable) {
        mRecycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(layoutManager);
        mRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, drawable));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
    }
}
