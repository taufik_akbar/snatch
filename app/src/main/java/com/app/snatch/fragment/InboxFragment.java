package com.app.snatch.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.activity.NotificationActivity;
import com.app.snatch.adapter.InboxAdapter;
import com.app.snatch.model.Message;
import com.app.snatch.presenter.InboxPresenter;
import com.app.snatch.views.InboxView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class InboxFragment extends BaseFragment implements InboxView {
    
    @BindView(R.id.mRecycler)
    RecyclerView mRecycler;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    
    private InboxPresenter inboxPresenter;
    
    public InboxFragment() {
        // Required empty public constructor
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_inbox;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setHasOptionsMenu(true);
        
        inboxPresenter = new InboxPresenter();
        inboxPresenter.attachView(this);
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        mToolbar.setTitle("Inbox");
        configList(mRecycler, android.R.color.transparent);
        
        
        inboxPresenter.getInbox(UserPreference.getToken(getActivity()));
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        inboxPresenter.detachView();
    }
    
    @OnClick(R.id.imgNotif)
    void notifListener() {
        startActivity(new Intent(getActivity(), NotificationActivity.class));
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_notif, menu);
    }
    
    
    @Override
    public void showProgressDialog() {
        showProgress();
    }
    
    @Override
    public void onInboxFail(String message) {
        dismissProgress();
    }
    
    @Override
    public void onInboxSuccess(ArrayList<Message> messages) {
        dismissProgress();
        System.out.println("SUCCESS");
        InboxAdapter adapter = new InboxAdapter(messages);
        mRecycler.setAdapter(adapter);
    }
}
