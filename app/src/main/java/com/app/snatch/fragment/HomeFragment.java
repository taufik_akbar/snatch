package com.app.snatch.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.activity.CreateListingActivity;
import com.app.snatch.activity.LoginActivity;
import com.app.snatch.activity.NotificationActivity;
import com.app.snatch.adapter.SnatchAdapter;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.model.Category;
import com.app.snatch.model.User;
import com.app.snatch.model.bus.CreateSnatchEvent;
import com.app.snatch.model.bus.StaticEvent;
import com.app.snatch.model.response.StaticResponse;
import com.app.snatch.presenter.ProfilePresenter;
import com.app.snatch.presenter.StaticPresenter;
import com.app.snatch.views.ProfileView;
import com.app.snatch.views.StaticView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements ProfileView, StaticView {
    
    @BindView(R.id.mRecycler)
    RecyclerView mRecycler;
    @BindView(R.id.rlProfile)
    RelativeLayout rlProfile;
    @BindView(R.id.imgProfile)
    CircleImageView imgProfile;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtTime)
    TextView txtTime;
    
    private ProfilePresenter profilePresenter;
    private StaticPresenter staticPresenter;
    private StaticResponse staticResponse;
    private ArrayList<Category> arrayCategory;
    
    public HomeFragment() {
        // Required empty public constructor
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        profilePresenter = new ProfilePresenter();
        profilePresenter.attachView(this);
        staticPresenter = new StaticPresenter();
        staticPresenter.attachView(this);
        
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
//        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        getActivity().getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        
        configList(mRecycler, android.R.color.transparent);
        if (UserPreference.getName(getActivity()).isEmpty()) {
            profilePresenter.getProfile(UserPreference.getToken(getActivity()));
        } else {
            setProfile(UserPreference.getName(getActivity()), UserPreference
                    .getProfPic(getActivity()));
        }
        
        staticPresenter.getStatic(UserPreference.getToken(getActivity()));
        
        
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        profilePresenter.detachView();
        staticPresenter.detachView();
        EventBus.getDefault().unregister(this);
    }
    
    @OnClick(R.id.btnAdd)
    void createListener() {
        
        Bundle bundle = new Bundle();
        bundle.putSerializable(STATIC_RESPONSE, staticResponse);
    
        for (int i = 0; i < staticResponse.getStaticData().getCategories().size(); i++) {
            System.out.println("CATEGORY NAME : " + staticResponse.getStaticData().getCategories().get(i).getCategoryName());
        }
        Intent intent = new Intent(getActivity(), CreateListingActivity.class);
        intent.putExtras(bundle);
//        startActivity(new Intent(HomeActivity.this, CreateListingActivity.class));
        startActivity(intent);
    }
    
    @Override
    public void showProgressDialog() {
    
    }
    
    @Override
    public void onProfileFail(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onProfileSuccess(User user) {
        UserPreference.setUser(getActivity(), user);
        setProfile(user.getName(), user.getImagePath().getImage());
        
    }

//    private void setProfile(String name, String image) {
//        txtName.setText(name);
//        Glide.with(getActivity()).load(image).apply(new RequestOptions().centerCrop()).into(imgProfile);
//    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_notif, menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_notification) {
            startActivity(new Intent(getActivity(), NotificationActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Subscribe
    public void onEventStatic(StaticEvent event) {
        if (event != null) {
        
        }
    }
    
    private void setProfile(String name, String image) {
        txtName.setText(name);
        String urlImage = ApiConfig.BASE_URL_IMAGE + image;
        System.out.println("URL IMAGE : " + urlImage);
        Glide.with(getActivity()).load(urlImage).apply(new RequestOptions().fitCenter())
                .into(imgProfile);
        
        Format formatter = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        String today = formatter.format(new Date());
//        txtTime.setText(today);
        System.out.println("Today : " + today);
    }
    
    @Override
    public void onStaticFail(String message) {
        
        System.out.println("MESSAGE FAILED : " + message);
        if (message.contains("401")) {
            UserPreference.signout(getActivity());
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
            Toast.makeText(getActivity(), "Session login end", Toast.LENGTH_LONG).show();
            
        }
    }
    
    @Override
    public void onStaticSuccess(StaticResponse user) {
        staticResponse = user;
//        Category category = new Category();
//        category.setCategoryId("3");
//        category.setCategoryName(LATEST_LISTING);
////        user.getStaticData().getCategories().add(category);
//        arrayCategory = user.getStaticData().getCategories();
//        arrayCategory.add(0, category);
        ArrayList<Category> arrayCaegorynew = user.getStaticData().getCategories();
        Collections.reverse(arrayCaegorynew);
//        ArrayList<Category> arrayCaegorynew = new ArrayList<>();
//        arrayCaegorynew.set(0, user.getStaticData().getCategories().get(1));
//        arrayCaegorynew.set(1, user.getStaticData().getCategories().get(0));
        
        System.out.println("CATEGORIES SIZE ARRAY  : " + user.getStaticData().getCategories());
        SnatchAdapter adapter = new SnatchAdapter(arrayCaegorynew, getFragmentManager());
        mRecycler.setAdapter(adapter);
    }
    
    @Subscribe
    public void onEventCreateSnatch(CreateSnatchEvent event) {
        if (event != null) {
            System.out.println("EVENT DISTRICT : ");
            staticPresenter.getStatic(UserPreference.getToken(getActivity()));
        }
    }
}
