package com.app.snatch.fragment;


import android.support.v4.app.Fragment;

import com.app.snatch.R;

import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateLocationFragment extends BaseFragment {
    
    
    public CreateLocationFragment() {
        // Required empty public constructor
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_create_location;
    }
    
    @OnClick(R.id.btnNext)
    void nextListener() {
        getActivity().finish();
    }
}
