package com.app.snatch.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.app.snatch.R;
import com.app.snatch.activity.NotificationActivity;
import com.app.snatch.adapter.MyListingTabAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyListingFragment extends BaseFragment {
    
    @BindView(R.id.mPager)
    ViewPager mPager;
    @BindView(R.id.mTabs)
    TabLayout mTabs;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    
    public MyListingFragment() {
        // Required empty public constructor
    }
    
    
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_my_listing, container, false);
//    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_my_listing;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setHasOptionsMenu(true);
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    
        mToolbar.setTitle("My listing");
        ArrayList<String> arrayTitle = new ArrayList<>();
        arrayTitle.add("My Snatch");
        arrayTitle.add("Owned List");
        arrayTitle.add("Draft");
        MyListingTabAdapter adapter = new MyListingTabAdapter(getChildFragmentManager(), arrayTitle);
    
        mPager.setOffscreenPageLimit(0);
        mPager.setAdapter(adapter);
    
        for (int i = 0; i < arrayTitle.size(); i++) {
            mTabs.addTab(mTabs.newTab().setText(arrayTitle.get(i)));
        }
    
        mTabs.setupWithViewPager(mPager);
    }
    
    @OnClick(R.id.imgNotif)
    void notifListener() {
        startActivity(new Intent(getActivity(), NotificationActivity.class));
    }
    
}
