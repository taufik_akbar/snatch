package com.app.snatch.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.snatch.R;
import com.app.snatch.model.Bedroom;
import com.app.snatch.model.LeaseTerm;
import com.app.snatch.model.Measurement;
import com.app.snatch.model.PriceType;
import com.app.snatch.model.PropertyType;
import com.app.snatch.model.SubType;
import com.app.snatch.model.Tenure;
import com.app.snatch.model.bus.DistrictEvent;
import com.app.snatch.model.bus.SearchAddressEvent;
import com.app.snatch.model.request.CreateSnatchRequest;
import com.app.snatch.model.response.StaticResponse;
import com.app.snatch.presenter.StaticPresenter;
import com.app.snatch.views.StaticView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateListingFragment extends BaseFragment implements StaticView {
    
    public static final String TAG = "CreateListingFragment";
    
    
    @BindView(R.id.edtPropertyName)
    EditText edtPropertyName;
    @BindView(R.id.edtPostalCode)
    EditText edtPostalCode;
    @BindView(R.id.edtAddress)
    EditText edtAddress;
    @BindView(R.id.edtPropertyGroupType)
    EditText edtPropertyGroup;
    @BindView(R.id.edtPropertySubType)
    EditText edtPropertySubtype;
    @BindView(R.id.edtPropertyPrice)
    EditText edtPrice;
    @BindView(R.id.edtPriceCouldbe)
    EditText edtPriceCouldbe;
    @BindView(R.id.edtTenure)
    EditText edtTenure;
    @BindView(R.id.edtBedroom)
    EditText edtBedroom;
    @BindView(R.id.edtFloor)
    EditText edtFloorArea;
    @BindView(R.id.edtMeasurementBuild)
    EditText edtMeasurement;
    @BindView(R.id.edtMeasurementLand)
    EditText edtMeasurementLand;
    @BindView(R.id.edtMeasurementFloor)
    EditText edtMeasurementFloor;
    @BindView(R.id.edtBuildup)
    EditText edtBuild;
    @BindView(R.id.edtLand)
    EditText edtLand;
    @BindView(R.id.edtBedroomEditable)
    EditText edtBedroomEditable;
    @BindView(R.id.edtMinimumLease)
    EditText edtMinimumLease;
    @BindView(R.id.edtBathroom)
    EditText edtBathroom;
    @BindView(R.id.edtDistrict)
    EditText edtDistrict;
    @BindView(R.id.txtBasic)
    TextView txtTitle;
    
    @BindView(R.id.tilPropertyGroupType)
    TextInputLayout tilPropertyGroupType;
    @BindView(R.id.tilPropertySubType)
    TextInputLayout tilPropertySubType;
    @BindView(R.id.tilLeaseTerm)
    TextInputLayout tilLeaseTerm;
    @BindView(R.id.tilTenure)
    TextInputLayout tilTenure;
    @BindView(R.id.tilPropertyName)
    TextInputLayout tilPropertyName;
    @BindView(R.id.tilBedroom)
    TextInputLayout tilBedroom;
    @BindView(R.id.tilMinimumLease)
    TextInputLayout tilMinimumLease;
    @BindView(R.id.tilBathroom)
    TextInputLayout tilBathroom;
    
    @BindView(R.id.lnBuildup)
    LinearLayout lnBuildup;
    @BindView(R.id.lnLandArea)
    LinearLayout lnLand;
    @BindView(R.id.lnFloorarea)
    LinearLayout lnFloorArea;
    @BindView(R.id.lnEdtbedroom)
    LinearLayout lnBedroomEditable;
//    @BindView(R.id.tilPropertyGroupType)
//    TextInputLayout tilPropertyGroupType;
//    @BindView(R.id.tilPropertySubType)
//    TextInputLayout tilPropertySubType;
    
    private StaticPresenter staticPresenter;
    private StaticResponse staticResponse;
    private CreateSnatchRequest createSnatchRequest;
    private ArrayList<PropertyType> arrayPropertyType;
    private ArrayList<SubType> arraySubtype;
    private ArrayList<PriceType> arrayPriceType;
    private ArrayList<Measurement> arrayMeasurement;
    private ArrayList<Measurement> arrayMeasurementLand;
    private ArrayList<Tenure> arrayTenure;
    private ArrayList<LeaseTerm> arrayLeaseTerm;
    private ArrayList<String> arrayPropertyTypeString;
    private ArrayList<String> arrayPropertySubTypeString;
    private ArrayList<Bedroom> arrayBedroom;
    private String type;
    
    public CreateListingFragment() {
        // Required empty public constructor
    }
    
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_create_listing;
    }
    
    public static CreateListingFragment newInstance(StaticResponse staticResponse, String title, CreateSnatchRequest request) {
        Bundle args = new Bundle();
        args.putSerializable(STATIC_RESPONSE, staticResponse);
        args.putSerializable(STATIC_REQUEST, request);
        args.putString(TITLE, title);
        CreateListingFragment fragment = new CreateListingFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (getArguments() != null) {
            staticResponse = (StaticResponse) getArguments().getSerializable(STATIC_RESPONSE);
            createSnatchRequest = (CreateSnatchRequest) getArguments()
                    .getSerializable(STATIC_REQUEST);
        }
        
        staticPresenter = new StaticPresenter();
        staticPresenter.attachView(this);
        
        arrayPropertyTypeString = new ArrayList<>();
        arrayPropertyTypeString.add("Retail");
        arrayPropertyTypeString.add("Office");
        arrayPropertyTypeString.add("Industrial");
        arrayPropertyTypeString.add("Land");
        
        arrayPropertySubTypeString = new ArrayList<>();
        arrayPropertySubTypeString.add("Mall Shop");
        arrayPropertySubTypeString.add("Shop/Shophouses");
        arrayPropertySubTypeString.add("Food & Beverages");
        arrayPropertySubTypeString.add("Medical");
        arrayPropertySubTypeString.add("Other Retail");
        
        
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        EventBus.getDefault().register(this);
//        staticPresenter.getStatic(UserPreference.getToken(getActivity()));
        System.out.println("CATEGORY : " + createSnatchRequest.getSnatchCategory());
        System.out.println("SNATCH TYPE : " + createSnatchRequest.getSubType());
        System.out.println("SNATCH PROPERTY GROUP : " + createSnatchRequest
                .getSnatchPropertyGroupType());
        arrayPropertyType = staticResponse.getStaticData().getPropertyType2();
        arraySubtype = staticResponse.getStaticData().getSubType2();
        arrayPriceType = staticResponse.getStaticData().getPriceType();
        arrayMeasurement = staticResponse.getStaticData().getMeasurement();
        arrayMeasurementLand = staticResponse.getStaticData().getMeasurement();
        arrayLeaseTerm = staticResponse.getStaticData().getLeaseTerm();
        
        
        String title = "";
        if (createSnatchRequest.getSnatchCategory().equalsIgnoreCase("Commercial")) {
            tilLeaseTerm.setVisibility(View.VISIBLE);
            tilTenure.setVisibility(View.GONE);
            title = "For " + createSnatchRequest.getWhatFor();
            if (title.equalsIgnoreCase(FOR_RENT)) {
                hideCommercial(true);
            } else {
                hideCommercial(false);
            }
    
            arrayTenure = staticResponse.getStaticData().getTenure2();
           
            
//            arrayPropertyType = new ArrayList<>();
//            PropertyType propertyType;
//            propertyType = new PropertyType();
//            propertyType.setGroupType("Retail");
//            propertyType.setGroupTypeId("1");
//            arrayPropertyType.add(propertyType);
//            propertyType = new PropertyType();
//            propertyType.setGroupType("Office");
//            propertyType.setGroupTypeId("2");
//            arrayPropertyType.add(propertyType);
//            propertyType = new PropertyType();
//            propertyType.setGroupType("Industrial");
//            propertyType.setGroupTypeId("3");
//            arrayPropertyType.add(propertyType);
//            propertyType = new PropertyType();
//            propertyType.setGroupType("Land");
//            propertyType.setGroupTypeId("4");
//            arrayPropertyType.add(propertyType);
//
//            arraySubtype = new ArrayList<>();
//            SubType subType;
//            subType = new SubType();
//            subType.setSubType("Mall Shop");
//            subType.setSubTypeId("1");
//            arraySubtype.add(subType);
//            subType = new SubType();
//            subType.setSubType("Shop/Shophouses");
//            subType.setSubTypeId("2");
//            arraySubtype.add(subType);
//            subType = new SubType();
//            subType.setSubType("Food & Beverages");
//            subType.setSubTypeId("3");
//            arraySubtype.add(subType);
//            subType = new SubType();
//            subType.setSubType("Medical");
//            subType.setSubTypeId("4");
//            arraySubtype.add(subType);
//            subType = new SubType();
//            subType.setSubType("Other Retail");
//            subType.setSubTypeId("5");
//            arraySubtype.add(subType);
        
        // RESIDENTIAL > HBD > SALE
        } else if (createSnatchRequest.getSnatchCategory()
                .equalsIgnoreCase("Residential") && createSnatchRequest.getSnatchPropertyGroupType()
                .equalsIgnoreCase("hdb")) {
            String type = createSnatchRequest.getSnatchPropertyGroupType();
            String available = createSnatchRequest.getWhatFor();
            title = type + " for " + available.toLowerCase();
            boolean status;
            if (available.equalsIgnoreCase("sale")) {
                status = true;
            } else {
                status = false;
            }
            hideResidentialSaleHDB(status);
        
        // RESIDENTIAL > SALE > PRIVATE
        } else if (createSnatchRequest.getSnatchCategory()
                .equalsIgnoreCase("Residential") && createSnatchRequest.getWhatFor()
                .equalsIgnoreCase("Sale") && createSnatchRequest.getSnatchPropertyGroupType()
                .equalsIgnoreCase("private")) {
            String subtype = createSnatchRequest.getSubType();
            String type = createSnatchRequest.getSnatchPropertyGroupType();
            String available = createSnatchRequest.getWhatFor();
            title = type + " " + subtype + " for " + available;
            boolean status;
            if (subtype.equalsIgnoreCase("condo")) {
                status = true;
    
                arrayBedroom = staticResponse.getStaticData().getBedroom1();
            } else {
                
                status = false;
                arrayBedroom = staticResponse.getStaticData().getBedroom2();
                
            }
    
            arrayTenure = staticResponse.getStaticData().getTenure1();
//            arrayTenure = new ArrayList<>();
//            Tenure tenure;
//            tenure = new Tenure();
//            tenure.setTenure("Freehold/99");
//            tenure.setTenureId("1");
//            arrayTenure.add(tenure);
//            tenure = new Tenure();
//            tenure.setTenure("Leasehold");
//            tenure.setTenureId("2");
//            arrayTenure.add(tenure);
//            tenure = new Tenure();
//            tenure.setTenure("Executive Condo");
//            tenure.setTenureId("3");
//            arrayTenure.add(tenure);
    
            hideResidentialSalePrivate(status);
        } else {
            String subtype = createSnatchRequest.getSubType();
            String type = createSnatchRequest.getSnatchPropertyGroupType();
            String available = createSnatchRequest.getWhatFor();
            title = type + " " + subtype + " for " + available;
    
            System.out.println("SUB TYPE : " + subtype);
            boolean status;
            if (subtype.equalsIgnoreCase("condo")) {
                status = true;
    
                arrayBedroom = staticResponse.getStaticData().getBedroom1();
            } else {
    
                status = false;
                arrayBedroom = staticResponse.getStaticData().getBedroom2();
    
    
            }
    
            arrayTenure = staticResponse.getStaticData().getTenure1();
//            arrayTenure = new ArrayList<>();
//            Tenure tenure;
//            tenure = new Tenure();
//            tenure.setTenure("Freehold/99");
//            tenure.setTenureId("1");
//            arrayTenure.add(tenure);
//            tenure = new Tenure();
//            tenure.setTenure("Leasehold");
//            tenure.setTenureId("2");
//            arrayTenure.add(tenure);
//            tenure = new Tenure();
//            tenure.setTenure("Executive Condo");
//            tenure.setTenureId("3");
//            arrayTenure.add(tenure);
            
            hideResidentialRentPrivate(status);
            
        }
        
        txtTitle.setText(title);
        
        edtMeasurement
                .setText(staticResponse.getStaticData().getMeasurement().get(1).getMeasurement());
        edtMeasurementLand
                .setText(staticResponse.getStaticData().getMeasurement().get(1).getMeasurement());
        edtMeasurementFloor
                .setText(staticResponse.getStaticData().getMeasurement().get(1).getMeasurement());
        
        
    }
    
    @OnClick(R.id.btnNext)
    void nextListener() {
        if (createSnatchRequest.getBuildMeasurementId() == null) {
            createSnatchRequest.setBuildMeasurementId(arrayMeasurement.get(1).getMeasurementId());
        }
    
        if (createSnatchRequest.getLandMeasurementId() == null) {
            createSnatchRequest.setLandMeasurementId(arrayMeasurement.get(1).getMeasurementId());
        }
        
        String address = edtAddress.getText().toString();
        String postalCode = edtPostalCode.getText().toString();
        String buildingName = edtPropertyName.getText().toString();
        String price = edtPrice.getText().toString();
        String buildMeasuremtn = edtBuild.getText().toString();
        String landMeasuremtn = edtLand.getText().toString();
        String bedroom = edtBedroomEditable.getText().toString();
        
        String floorArea = edtFloorArea.getText().toString();
        String bathroom = edtBathroom.getText().toString();
        String whatFor = createSnatchRequest.getWhatFor();
        String category = createSnatchRequest.getSnatchCategory();
        String groupType = createSnatchRequest.getSnatchPropertyGroupType();
        String subType = createSnatchRequest.getSubType();
        String district = createSnatchRequest.getDistrictId();
        String tenure = createSnatchRequest.getTenureID();
        String leaseTermChoose = createSnatchRequest.getLeaseTermId();
        String bedroomChoose = createSnatchRequest.getBedroomId();
        
        System.out.println("WHAT FOR : " + whatFor);
        System.out.println("CATEGORY  : " + category);
        System.out.println("GROUP TYPE  : " + groupType);
    
        if (postalCode.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill postal code", Toast.LENGTH_SHORT).show();
            return;
        } else if (address.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill address", Toast.LENGTH_SHORT).show();
            return;
        } else if (district == null) {
            Toast.makeText(getActivity(), "Please fll district", Toast.LENGTH_SHORT).show();
            return;
        } else if (category.equalsIgnoreCase(COMMERCIAL)
                && groupType.isEmpty()) {
            Toast.makeText(getActivity(), "Please choose property group type", Toast.LENGTH_SHORT).show();
            return;
        } else if (category.equalsIgnoreCase(COMMERCIAL)
                && subType.isEmpty()) {
            Toast.makeText(getActivity(), "Please choose property sub type", Toast.LENGTH_SHORT).show();
            return;
        } else if (price.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill price", Toast.LENGTH_SHORT).show();
            return;
        } else if (whatFor.equalsIgnoreCase(RENT)
                && groupType.equalsIgnoreCase(HDB) && leaseTermChoose == null) {
            Toast.makeText(getActivity(), "Please choose lease term", Toast.LENGTH_SHORT).show();
            return;
            // sale > private > condo
        } else if ((whatFor.equalsIgnoreCase(SALE) || whatFor.equalsIgnoreCase(RENT))
                && groupType.equalsIgnoreCase(PRIVATE)
                && (subType.equalsIgnoreCase(CONDO))
                && tenure == null) {
            Toast.makeText(getActivity(), "Please choose tenure", Toast.LENGTH_SHORT).show();
            return;
        } else if ((whatFor.equalsIgnoreCase(SALE) || whatFor.equalsIgnoreCase(RENT))
                && groupType.equalsIgnoreCase(PRIVATE)
                && (subType.equalsIgnoreCase(CONDO) || subType.equalsIgnoreCase(LANDED))
                && bedroomChoose == null) {
            Toast.makeText(getActivity(), "Please choose bedroom", Toast.LENGTH_SHORT).show();
            return;
        } else if (groupType.equalsIgnoreCase(HDB) && bedroom.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill bedroom", Toast.LENGTH_SHORT).show();
            return;
        } else if (category.equalsIgnoreCase(RESIDENTIAL) && (whatFor.equalsIgnoreCase(SALE) || whatFor.equalsIgnoreCase(RENT)) &&
                groupType.equalsIgnoreCase(PRIVATE) && bathroom.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill bathroom", Toast.LENGTH_SHORT).show();
            return;
        } else if (category.equalsIgnoreCase(RESIDENTIAL) && whatFor.equalsIgnoreCase(SALE) &&
                groupType.equalsIgnoreCase(HDB) && bathroom.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill bathroom", Toast.LENGTH_SHORT).show();
            return;
        } else if ((groupType.equalsIgnoreCase(HDB) && floorArea.isEmpty())
                || (subType != null && subType.equalsIgnoreCase(CONDO) && floorArea.isEmpty())) {
            Toast.makeText(getActivity(), "Please fill floor area", Toast.LENGTH_SHORT).show();
            return;
        } else if (category.equalsIgnoreCase(COMMERCIAL) || (whatFor.equalsIgnoreCase(SALE) || whatFor.equalsIgnoreCase(RENT))
                && groupType.equalsIgnoreCase(PRIVATE)
                && subType.equalsIgnoreCase(LANDED)
                && buildMeasuremtn.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill build up area", Toast.LENGTH_SHORT).show();
            return;
        } else if (category.equalsIgnoreCase(COMMERCIAL) || (whatFor.equalsIgnoreCase(SALE) || whatFor.equalsIgnoreCase(RENT))
                && groupType.equalsIgnoreCase(PRIVATE)
                && subType.equalsIgnoreCase(LANDED)
                && landMeasuremtn.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill land up area", Toast.LENGTH_SHORT).show();
            return;
        } else if (category.equalsIgnoreCase(COMMERCIAL) && buildingName.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill building name", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "validate", Toast.LENGTH_SHORT).show();
    
            createSnatchRequest.setAddress(address);
            createSnatchRequest.setPostalCode(postalCode);
            createSnatchRequest.setSnatchName(buildingName);
            createSnatchRequest.setSnatchPrice(price);
            createSnatchRequest.setSnatchFloorArea(edtFloorArea.getText().toString());
            createSnatchRequest.setSnatchBedroom(edtBedroom.getText().toString());
            createSnatchRequest.setBuildMeasurement(buildMeasuremtn);
            createSnatchRequest.setLandMeasurement(landMeasuremtn);
            createSnatchRequest.setLocationFloorId("1");
            createSnatchRequest.setStatusId("1");
            createSnatchRequest.setPriceTypeId("1");
            createSnatchRequest.setListTypeId("1");
            createSnatchRequest.setImageId("1");
    
            loadReplaceFragment(R.id.container_create, CreatePhotoFragment
                    .newInstance(createSnatchRequest, staticResponse), CreatePhotoFragment.class.getName());
        }
        
//        else if (category.equalsIgnoreCase("commercial") && landMeasuremtn.isEmpty()) {
//            Toast.makeText(getActivity(), "Please fill land area", Toast.LENGTH_SHORT).show();
//        } else if (category.equalsIgnoreCase("commercial") && landMeasuremtn.isEmpty()) {
//            Toast.makeText(getActivity(), "Please fill build up area", Toast.LENGTH_SHORT).show();
//        }
    
    
    
    
        
    }
    
    @OnClick(R.id.edtPostalCode)
    void postalCodeListener() {
        loadReplaceFragment(R.id.container_create, new SearchZipCodeFragment(), SearchZipCodeFragment.class
                .getName());
    }
    
    @OnClick(R.id.edtDistrict)
    void districtListener() {
        loadReplaceFragment(R.id.container_create, DistrictFragment.newInstance(staticResponse.getStaticData().getDistricts()), DistrictFragment.class
                .getName());
    }
    
    @OnClick(R.id.edtPropertyGroupType)
    void groupTypeListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayPropertyType.size(); i++) {
            list.add(arrayPropertyType.get(i).getGroupType());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);

        showDialogList(items, edtPropertyGroup);
    }
    
    @OnClick(R.id.edtPropertySubType)
    void subTypeListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arraySubtype.size(); i++) {
            list.add(arraySubtype.get(i).getSubType());
        }

        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtPropertySubtype);

    }
    
    @OnClick(R.id.edtMinimumLease)
    void minimumLeaseListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayLeaseTerm.size(); i++) {
            list.add(arrayLeaseTerm.get(i).getLeaseTerm());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtMinimumLease);
        
    }
    
    @OnClick(R.id.edtBedroom)
    void bedroomListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayBedroom.size(); i++) {
            list.add(arrayBedroom.get(i).getBedroom());
        }

//        CharSequence[] items = list.toArray(new CharSequence[list.size()]);


//        if (type.equalsIgnoreCase(COMMERCIAL_SALE)) {
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtBedroom);
//        } else {
//            CharSequence[] items = list.toArray(new CharSequence[list.size()]);
//            showDialogList(items, edtPropertySubtype);
//        }
    }
    
    //    @OnClick(R.id.edtPostalCode)
//    void postalCodeListener() {
//        loadReplaceFragment(R.id.container_create, , CreateListingFragment.class.getName());
//    }
//
    @OnClick(R.id.edtPriceCouldbe)
    void priceTypeListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayPriceType.size(); i++) {
            list.add(arrayPriceType.get(i).getPriceType());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtPriceCouldbe);
    }
    
    
    @OnClick(R.id.edtMeasurementBuild)
    void measurementListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayMeasurement.size(); i++) {
            list.add(arrayMeasurement.get(i).getMeasurement());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtMeasurement);
    }
    
    @OnClick(R.id.edtMeasurementLand)
    void measurementLandListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayMeasurementLand.size(); i++) {
            list.add(arrayMeasurementLand.get(i).getMeasurement());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtMeasurementLand);
    }
    
    @OnClick(R.id.edtMeasurementFloor)
    void measurementFloorListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayMeasurementLand.size(); i++) {
            list.add(arrayMeasurementLand.get(i).getMeasurement());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtMeasurementFloor);
    }
    
    @OnClick(R.id.edtTenure)
    void tenureListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayTenure.size(); i++) {
            list.add(arrayTenure.get(i).getTenure());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtTenure);
    }
    
    @OnClick(R.id.btnSave)
    void saveListener() {
    
    }
    
    @Override
    public void showProgressDialog() {
    
    }
    
    @Override
    public void onStaticFail(String message) {
    
    }
    
    @Override
    public void onStaticSuccess(StaticResponse staticResponse) {
        
        String listType = staticResponse.getStaticData().getCategories().get(0).getCategoryName();
        
        System.out.println("LIST TYPE : " + listType);
        edtMeasurement
                .setText(staticResponse.getStaticData().getMeasurement().get(0).getMeasurement());
        edtMeasurementLand
                .setText(staticResponse.getStaticData().getMeasurement().get(0).getMeasurement());
        
        arrayPropertyType = staticResponse.getStaticData().getPropertyType1();
        arraySubtype = staticResponse.getStaticData().getSubType();
        arrayPriceType = staticResponse.getStaticData().getPriceType();
        arrayMeasurement = staticResponse.getStaticData().getMeasurement();
        arrayMeasurementLand = staticResponse.getStaticData().getMeasurement();
        arrayTenure = staticResponse.getStaticData().getTenure1();
    }
    
    private void showDialogList(final CharSequence[] items, final EditText editText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                editText.setText(items[which]);
                
                if (editText.getId() == R.id.edtPropertyGroupType) {
                    createSnatchRequest.setPropertyGroupId(arrayPropertyType.get(which).getGroupTypeId());
                } else if (editText.getId() == R.id.edtMeasurementLand) {
                    createSnatchRequest.setLandMeasurementId(arrayMeasurementLand.get(which).getMeasurementId());
                } else if (editText.getId() == R.id.edtMeasurementBuild) {
                    createSnatchRequest.setBuildMeasurementId(arrayMeasurementLand.get(which).getMeasurementId());
                } else if (editText.getId() == R.id.edtMeasurementFloor) {
                    createSnatchRequest.setBuildMeasurementId(arrayMeasurementLand.get(which).getMeasurementId());
                } else if (editText.getId() == R.id.edtPropertySubType) {
                    createSnatchRequest.setSubTypeId(arraySubtype.get(which).getSubTypeId());
                } else if (editText.getId() == R.id.edtMinimumLease) {
                    createSnatchRequest.setLeaseTermId(arrayLeaseTerm.get(which).getLeaseTermId());
                } else if (editText.getId() == R.id.edtBedroom) {
                    createSnatchRequest.setBedroomId(arrayBedroom.get(which).getBedroomId());
                } else if (editText.getId() == R.id.edtTenure) {
                    System.out.println("POSITION TENURE : " + which);
                    createSnatchRequest.setTenureID(String.valueOf(which+1));
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }
    
    private void hideResidentialSaleHDB(boolean available) {
        tilPropertySubType.setVisibility(View.GONE);
        tilPropertyGroupType.setVisibility(View.GONE);
        tilLeaseTerm.setVisibility(View.GONE);
        tilPropertyName.setVisibility(View.GONE);
        tilTenure.setVisibility(View.GONE);
        lnBuildup.setVisibility(View.GONE);
        lnLand.setVisibility(View.GONE);
        tilBedroom.setVisibility(View.GONE);
        
        if (!available) {
            tilMinimumLease.setVisibility(View.VISIBLE);
            tilBathroom.setVisibility(View.GONE);
        } else {
            tilMinimumLease.setVisibility(View.GONE);
            tilBathroom.setVisibility(View.VISIBLE);
        }
        
    }
    
    private void hideResidentialSalePrivate(boolean condo) {
        tilPropertySubType.setVisibility(View.GONE);
        tilPropertyGroupType.setVisibility(View.GONE);
        tilLeaseTerm.setVisibility(View.GONE);
        tilPropertyName.setVisibility(View.GONE);
        tilMinimumLease.setVisibility(View.GONE);
        lnBedroomEditable.setVisibility(View.GONE);
        lnBuildup.setVisibility(View.GONE);
        lnLand.setVisibility(View.GONE);
        if (condo) {
            tilTenure.setVisibility(View.VISIBLE);
        } else {
            tilTenure.setVisibility(View.GONE);
            lnBuildup.setVisibility(View.VISIBLE);
            lnLand.setVisibility(View.VISIBLE);
            lnFloorArea.setVisibility(View.GONE);
        }
    }
    
    private void hideResidentialRentPrivate(boolean condo) {
        tilPropertySubType.setVisibility(View.GONE);
        tilPropertyGroupType.setVisibility(View.GONE);
        tilLeaseTerm.setVisibility(View.GONE);
        tilPropertyName.setVisibility(View.GONE);
        tilMinimumLease.setVisibility(View.GONE);
        lnBedroomEditable.setVisibility(View.GONE);
        lnBuildup.setVisibility(View.GONE);
        lnLand.setVisibility(View.GONE);
        if (condo) {
            tilTenure.setVisibility(View.VISIBLE);
        } else {
            tilTenure.setVisibility(View.GONE);
            lnBuildup.setVisibility(View.VISIBLE);
            lnLand.setVisibility(View.VISIBLE);
            lnFloorArea.setVisibility(View.GONE);
        }
    }
    
    private void hideCommercial(boolean condo) {
        tilMinimumLease.setVisibility(View.GONE);
        lnBedroomEditable.setVisibility(View.GONE);
        tilBedroom.setVisibility(View.GONE);
        tilBathroom.setVisibility(View.GONE);
        lnFloorArea.setVisibility(View.GONE);
        
        if (!condo) {
            tilLeaseTerm.setVisibility(View.GONE);
            tilTenure.setVisibility(View.VISIBLE);
        } else {
            tilLeaseTerm.setVisibility(View.VISIBLE);
            tilTenure.setVisibility(View.GONE);
        }
    }
    
    @Subscribe
    public void onEventDistrict(DistrictEvent event) {
        if (event != null) {
            System.out.println("get district name : " + event.getDistrict());
            edtDistrict.setText(event.getDistrict());
            createSnatchRequest.setDistrictId(event.getDistrictId());
        }
    }
    
    @Subscribe
    public void onEventSearch(SearchAddressEvent event) {
        if (event != null) {
            System.out.println("EVENT DISTRICT : ");
            edtAddress.setText(event.getPostalCode());
            edtPostalCode.setText(event.getAddress());
        }
    }
}
