package com.app.snatch.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.snatch.utils.Constants;
import com.app.snatch.utils.DividerItemDecoration;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public abstract class BaseFragment extends Fragment implements Constants {

    protected Unbinder unbinder;
    protected ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    protected abstract int getFragmentLayout();

    private void injectViews(final View view) {
        unbinder = ButterKnife.bind(this, view);
    }

    protected void loadFragment(int container, Fragment fragment, String tag) {
        getFragmentManager().beginTransaction()
                .add(container, fragment, tag)
                .addToBackStack(tag).commit();
    }
    
    protected void loadReplaceFragment(int container, Fragment fragment, String tag) {
        getFragmentManager().beginTransaction()
                .add(container, fragment, tag)
                .addToBackStack(tag).commit();
    }

    protected void configList(RecyclerView mRecycler, int lineColor) {
        mRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(manager);
        mRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, lineColor));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
    }
    
    protected void configListHorizontal(RecyclerView mRecycler, int lineColor) {
        mRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(manager);
        mRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL, lineColor));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
    }
    
    protected void configListHeight(RecyclerView mRecycler, int lineColor, int height) {
        mRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(manager);
        mRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, lineColor, height));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
    }

    protected void configListWithoutDivider(RecyclerView mRecycler, int lineColor) {
        mRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(manager);
        mRecycler.setItemAnimator(new DefaultItemAnimator());
    }

//    protected void configStaggerd(RecyclerView mRecycler) {
//        mRecycler.setHasFixedSize(true);
//        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(2, 1);
//        mRecycler.setLayoutManager(manager);
//
//        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.space);
//        mRecycler.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
//        mRecycler.setItemAnimator(new DefaultItemAnimator());
//    }

    protected void showProgress() {
        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setMessage(getResources().getString(R.string.prog_dialog_send));
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    protected void dismissProgress() {
        progressDialog.dismiss();
    }

}
