package com.app.snatch.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.snatch.LocationPreference;
import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.model.Tenanted;
import com.app.snatch.model.bus.CreateSnatchEvent;
import com.app.snatch.model.bus.LocationEvent;
import com.app.snatch.model.request.CreateSnatchRequest;
import com.app.snatch.model.response.StaticResponse;
import com.app.snatch.presenter.CreateSnatchPresenter;
import com.app.snatch.views.DefaultView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateExtraFragment extends BaseFragment implements DefaultView {
    
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.btnNext)
    Button btnNext;
    
    @BindView(R.id.btnAmenities)
    RelativeLayout rlAmenities;
    @BindView(R.id.btnNearby)
    RelativeLayout rlNearby;
    @BindView(R.id.tilTenanted)
    TextInputLayout tilTenanted;
    @BindView(R.id.tilCondition)
    TextInputLayout tilCondition;
    @BindView(R.id.tilUnitOffered)
    TextInputLayout tilUnitOffered;
    @BindView(R.id.edtUnitOffered)
    EditText edtUnitOffered;
    @BindView(R.id.edtCondition)
    EditText edtCondition;
    @BindView(R.id.edtTenanted)
    EditText edtTenanted;
    @BindView(R.id.edtListDesc)
    EditText edtListDesc;
    @BindView(R.id.dividierNearby)
    View dividerNearby;
    @BindView(R.id.dividierAmenities)
    View dividerAmenities;
    @BindView(R.id.txtUnitDetails)
    TextView txtUnitDetail;
    
    private ArrayList<Tenanted> arrayTenanted;
    private ArrayList<String> arrayCondition;
    private ArrayList<String> arrayUnitOffered;
    
    private CreateSnatchRequest createSnatchRequest;
    private CreateSnatchPresenter createSnatchPresenter;
    private StaticResponse staticResponse;
    
    public CreateExtraFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_create_extra;
    }
    
    public static CreateExtraFragment newInstance(CreateSnatchRequest request, StaticResponse response) {
        Bundle args = new Bundle();
        args.putSerializable(STATIC_REQUEST, request);
        args.putSerializable(STATIC_RESPONSE, response);
        CreateExtraFragment fragment = new CreateExtraFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        createSnatchPresenter = new CreateSnatchPresenter();
        createSnatchPresenter.attachView(this);
        
        if (getArguments() != null) {
            createSnatchRequest = (CreateSnatchRequest) getArguments()
                    .getSerializable(STATIC_REQUEST);
            staticResponse = (StaticResponse) getArguments().getSerializable(STATIC_RESPONSE);
        }
        
        arrayTenanted = staticResponse.getStaticData().getTenanteds();
//        arrayTenanted.add("Yes");
//        arrayTenanted.add("No");
    
        arrayCondition = new ArrayList<>();
        arrayCondition.add("Brand New");
        arrayCondition.add("For Reno / A&A");
        arrayCondition.add("Land Only & Rebuilding");
    
        arrayUnitOffered = new ArrayList<>();
        arrayUnitOffered.add("Fully Furnished");
        arrayUnitOffered.add("Partially Furnished");
        arrayUnitOffered.add("Unfurnished");
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSave.setVisibility(View.VISIBLE);
        btnNext.setText("Create");
        
        String category = createSnatchRequest.getSnatchCategory();
        String available = createSnatchRequest.getWhatFor();
        if (category.equalsIgnoreCase(COMMERCIAL)) {
            hideCommercial();
        } else {
            String groupType = createSnatchRequest.getSnatchPropertyGroupType();
            String type;
            if (available.equalsIgnoreCase(SALE)) {
                
                
                if (groupType.equalsIgnoreCase(HDB)) {
                    type = groupType;
                } else {
                    type = createSnatchRequest.getSubType();
                }
                hideResidentialSale(type);
            } else {
                if (groupType.equalsIgnoreCase(HDB)) {
                    type = groupType;
                } else {
                    type = createSnatchRequest.getSubType();
                }
                hideResidentialRent(type);
            }
        }
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    
    @OnClick(R.id.btnAmenities)
    void amenitiesListener() {
        loadFragment(R.id.container_create, CreateAmenitiesFragment.newInstance(AMENITIES), CreateAmenitiesFragment.class.getName());
    }
    
    @OnClick(R.id.btnNearby)
    void propertyListener() {
        loadFragment(R.id.container_create, CreateAmenitiesFragment.newInstance(PROPERTY_NEARBY), CreateAmenitiesFragment.class.getName());
    }
    
    @OnClick(R.id.btnNext)
    void nextListener() {
        createSnatchRequest.setSnatchDescription(edtListDesc.getText().toString());
//        loadFragment(R.id.container_create, new CreateLocationFragment(), CreateLocationFragment.class.getName());
//        getActivity().finish();
        post();
    }
    
    @OnClick(R.id.edtUnitOffered)
    void unitOfferedListener() {
        CharSequence[] items = arrayUnitOffered.toArray(new CharSequence[arrayUnitOffered.size()]);
        showDialogList(items, edtUnitOffered);
    }
    
    @OnClick(R.id.edtTenanted)
    void tenantedListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayTenanted.size(); i++) {
            list.add(arrayTenanted.get(i).getTenanted());
        }
    
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtTenanted);
//        CharSequence[] items = arrayTenanted.toArray(new CharSequence[arrayTenanted.size()]);
        showDialogList(items, edtTenanted);
    }
    
    @OnClick(R.id.edtCondition)
    void conditionListener() {
        CharSequence[] items = arrayCondition.toArray(new CharSequence[arrayCondition.size()]);
        showDialogList(items, edtCondition);
    }
    
    private void showDialogList(final CharSequence[] items, final EditText editText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                editText.setText(items[which]);
                
                if (editText.getId() == R.id.edtTenanted) {
                    createSnatchRequest.setTenantedId(arrayTenanted.get(which).getTenantedId());
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }
    
    private void hideCommercial() {
        tilTenanted.setVisibility(View.GONE);
        tilCondition.setVisibility(View.GONE);
        tilUnitOffered.setVisibility(View.GONE);
    }
    
    private void hideResidentialSale(String type) {
    
        
        tilUnitOffered.setVisibility(View.GONE);
        
        
        if (type.equalsIgnoreCase("condo")) {
            tilCondition.setVisibility(View.GONE);
        } else if (type.equalsIgnoreCase("landed")){
            tilTenanted.setVisibility(View.GONE);
            tilCondition.setVisibility(View.VISIBLE);
        } else {
            tilTenanted.setVisibility(View.GONE);
            tilCondition.setVisibility(View.GONE);
            rlAmenities.setVisibility(View.GONE);
            rlNearby.setVisibility(View.GONE);
            dividerAmenities.setVisibility(View.GONE);
            dividerNearby.setVisibility(View.GONE);
            txtUnitDetail.setVisibility(View.GONE);
        }
    
        
    }
    
    private void hideResidentialRent(String type) {
        tilTenanted.setVisibility(View.GONE);
        tilCondition.setVisibility(View.GONE);
//        rlAmenities.setVisibility(View.GONE);
//        rlNearby.setVisibility(View.GONE);
//        dividerAmenities.setVisibility(View.GONE);
//        dividerNearby.setVisibility(View.GONE);
    
        if (type.equalsIgnoreCase("HBD")) {
            rlAmenities.setVisibility(View.GONE);
            rlNearby.setVisibility(View.GONE);
            dividerAmenities.setVisibility(View.GONE);
            dividerNearby.setVisibility(View.GONE);
            txtUnitDetail.setVisibility(View.GONE);
        }
    }
    
    private void post() {
    
        System.out.println("category id : " + createSnatchRequest.getCategoryId());
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("category_id", createSnatchRequest.getCategoryId())
                .addFormDataPart("what_for_id", createSnatchRequest.getWhatForId())
                .addFormDataPart("postal_code", createSnatchRequest.getPostalCode())
                .addFormDataPart("address", createSnatchRequest.getAddress())
                .addFormDataPart("district_id", createSnatchRequest.getDistrictId())
                .addFormDataPart("x", "1")
                .addFormDataPart("y", "1")
                .addFormDataPart("latitude", LocationPreference.getLatitude(getActivity()))
                .addFormDataPart("longitude", LocationPreference.getLongitude(getActivity()))
                .addFormDataPart("description", createSnatchRequest.getSnatchDescription())
                .addFormDataPart("price", createSnatchRequest.getSnatchPrice())
                .addFormDataPart("built_area", createSnatchRequest.getBuildMeasurement())
                .addFormDataPart("built_measurement_id", createSnatchRequest.getBuildMeasurementId())
                .addFormDataPart("measurement_id", createSnatchRequest.getBuildMeasurementId())
                .addFormDataPart("land_area", createSnatchRequest.getLandMeasurement())
                .addFormDataPart("land_measurement_id", createSnatchRequest.getLandMeasurementId())
                .addFormDataPart("description", createSnatchRequest.getSnatchDescription())
                .addFormDataPart("group_type_id", createSnatchRequest.getPropertyGroupId())
                .addFormDataPart("sub_type_id", createSnatchRequest.getSubTypeId())
                .addFormDataPart("tenure_id", createSnatchRequest.getTenureID());
//                .addFormDataPart("bedrooms", createSnatchRequest.getSnatchBedroom())
//                .addFormDataPart("bedroom", createSnatchRequest.getUnitDetailId())
//                .addFormDataPart("bathroom", createSnatchRequest.getBathroom())
//                .addFormDataPart("lease_term_id", createSnatchRequest.getLeaseTermId())
//                .addFormDataPart("tenanted_id", createSnatchRequest.getUnitDetailId())
//                .addFormDataPart("unit_detail_id", createSnatchRequest.getUnitDetailId());

    
        builder.addFormDataPart("facilities[0]", "1");
        builder.addFormDataPart("amenities[0]", "1");
        
        for (int i = 0; i < createSnatchRequest.getArrayImage().size(); i++) {
            builder.addFormDataPart("images[" + i + "]", createSnatchRequest.getArrayImage().get(i));
        }
        
        createSnatchPresenter.createSnatch(UserPreference.getToken(getActivity()), builder.build());
        
    }
    
    @Override
    public void showProgressDialog() {
        showProgress();
    }
    
    @Override
    public void onDefaultFail(String message) {
        dismissProgress();
    }
    
    @Override
    public void onDefaultSuccess(String message) {
        dismissProgress();
        EventBus.getDefault().post(new CreateSnatchEvent());
        getActivity().finish();
    }
    
    @Subscribe
    public void onEventLocation(LocationEvent event) {
        
        if (event != null) {
            
            createSnatchRequest.setLatitude(String.valueOf(event.getCurrentLatitude()));
            createSnatchRequest.setLongitude(String.valueOf(event.getCurrentLongitude()));
            
        }
    }
}
