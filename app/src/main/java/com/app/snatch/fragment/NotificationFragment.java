package com.app.snatch.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.snatch.R;
import com.app.snatch.adapter.NotificationAdapter;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment {
    
    public static final String TAG = NotificationFragment.class.getName();
    
    @BindView(R.id.mRecycler)
    RecyclerView mRecycler;
    
    public NotificationFragment() {
        // Required empty public constructor
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_notification;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    
        configListHeight(mRecycler, android.R.color.transparent, 16);
    
    
        NotificationAdapter adapter = new NotificationAdapter(new ArrayList<String>(), getFragmentManager());
        mRecycler.setAdapter(adapter);
    }
    
}
