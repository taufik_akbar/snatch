package com.app.snatch.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.snatch.R;
import com.app.snatch.adapter.SearchAddressAdapter;
import com.app.snatch.api.ApiService;
import com.app.snatch.model.bus.SearchAddressEvent;
import com.app.snatch.model.response.LocationResponse;
import com.google.android.gms.common.api.GoogleApiClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchZipCodeFragment extends BaseFragment {
    
    @BindView(R.id.edtSearch)
    EditText edtSearch;
    @BindView(R.id.mRecycler)
    RecyclerView mRecycler;
    
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<LocationResponse.Data> arrayData;
    private SearchAddressAdapter addressAdapter;
    
    public SearchZipCodeFragment() {
        // Required empty public constructor
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_search_zip_code;
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        configList(mRecycler, R.color.transparent);
        arrayData = new ArrayList<>();
        addressAdapter = new SearchAddressAdapter(arrayData);
        mRecycler.setAdapter(addressAdapter);
        
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();
        
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/api/geocode/").client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();
        final ApiService service = retrofit.create(ApiService.class);
        
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    
            }
    
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        
            }
    
            @Override
            public void afterTextChanged(Editable editable) {
                service.getLocation("postal_code:" + editable.toString(), "street_address", "AIzaSyCVn7UetonPiAmOZMb2IVBsfm2AxYXZwqQ")
                        .enqueue(new Callback<LocationResponse>() {
                            //                service.getLocation().enqueue(new Callback<LocationResponse>() {
                            @Override
                            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                                LocationResponse data = response.body();
                
                                if (data.getStatus().equalsIgnoreCase("ok")) {
                                    System.out.println("MASUK SET ADAPTER ");
                                    System.out.println("MASUK SET ADAPTER SIZE ARRAY " + data.getDataArrayList().size());
                                    addressAdapter.setData(data.getDataArrayList());
                                } else {
                                    Toast.makeText(getActivity(), "no data result", Toast.LENGTH_SHORT)
                                            .show();
                                }
                
                            }
            
                            @Override
                            public void onFailure(Call<LocationResponse> call, Throwable t) {
                
                            }
                        });
            }
        });
    
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    
    @Subscribe
    public void onEventSearch(SearchAddressEvent event) {
        if (event != null) {
            System.out.println("EVENT DISTRICT : ");
            getFragmentManager().popBackStackImmediate();
        }
    }

}
