package com.app.snatch.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.adapter.MyListingSnatchAdapter;
import com.app.snatch.model.Snatch;
import com.app.snatch.presenter.SnatchOwnedPresenter;
import com.app.snatch.views.OwnedView;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyListingSnatchFragment extends BaseFragment implements OwnedView {
    
    @BindView(R.id.mRecycler)
    RecyclerView mRecycler;
    
    private String type;
    private SnatchOwnedPresenter snatchOwnedPresenter;
    
    public MyListingSnatchFragment() {
        // Required empty public constructor
    }
    
    public static MyListingSnatchFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString(TYPE, type);
        MyListingSnatchFragment fragment = new MyListingSnatchFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_my_listing_snatch;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (getArguments() != null) {
            type = getArguments().getString(TYPE);
        }
        
        snatchOwnedPresenter = new SnatchOwnedPresenter();
        snatchOwnedPresenter.attachView(this);
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        
        configList(mRecycler, android.R.color.transparent);
        
        if (type.equals(OWNED_LIST)) {
            snatchOwnedPresenter.getOwned(UserPreference.getToken(getActivity()));
        } else {
            snatchOwnedPresenter.getList(UserPreference.getToken(getActivity()));
        }
        
        
    }
    
    @Override
    public void showProgressDialog() {
    
    }
    
    @Override
    public void onOwnedFail(String message) {
    
    }
    
    @Override
    public void onOwnedSuccess(ArrayList<Snatch> snatches) {
        MyListingSnatchAdapter adapter = new MyListingSnatchAdapter(snatches, type);
        mRecycler.setAdapter(adapter);
    }
}
