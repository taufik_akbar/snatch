package com.app.snatch.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.app.snatch.R;
import com.app.snatch.model.SubType;
import com.app.snatch.model.WhatFor;
import com.app.snatch.model.Category;
import com.app.snatch.model.PropertyType;
import com.app.snatch.model.bus.LocationEvent;
import com.app.snatch.model.request.CreateSnatchRequest;
import com.app.snatch.model.response.StaticResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateListingCategoryFragment extends BaseFragment {
    
    @BindView(R.id.edtCategory)
    EditText edtCategory;
    @BindView(R.id.edtPropertyGroupType)
    EditText edtGroupType;
    @BindView(R.id.edtPropertySubTypes)
    EditText edtGroupTypes;
    @BindView(R.id.tilPropertyGroupType)
    TextInputLayout tilPropertyGroupType;
    @BindView(R.id.tilPropertyGroupTypes)
    TextInputLayout tilPropertyGroupTypes;
    @BindView(R.id.rgTypeSale)
    RadioGroup rgAvailable;
    
    private ArrayList<Category> arrayCategory;
    private ArrayList<PropertyType> arrayPropertyType;
    private ArrayList<SubType> arraySubType;
    private ArrayList<WhatFor> arrayWhatFor;
    private StaticResponse staticResponse;
    private CreateSnatchRequest createRequest;
    
    public CreateListingCategoryFragment() {
        // Required empty public constructor
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_create_listing_category;
    }
    
    public static CreateListingCategoryFragment newInstance(StaticResponse staticResponse) {
        Bundle args = new Bundle();
        args.putSerializable(STATIC_RESPONSE, staticResponse);
        CreateListingCategoryFragment fragment = new CreateListingCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        
        if (getArguments() != null) {
            staticResponse = (StaticResponse) getArguments().getSerializable(STATIC_RESPONSE);
            arrayCategory = staticResponse.getStaticData().getCategories();
            arrayWhatFor = staticResponse.getStaticData().getWhatFors();
            arrayPropertyType = staticResponse.getStaticData().getPropertyType1();
            arraySubType = staticResponse.getStaticData().getSubType();
            System.out.println("SUB TYPE SIZE : " + arraySubType.size());
            
            //            arraySubType = new ArrayList<>();
//
//            PropertyType propertyType;
//            propertyType = new PropertyType();
//            propertyType.setGroupTypeId("1");
//            propertyType.setGroupType("Condo");
//            arraySubType.add(propertyType);
//            propertyType = new PropertyType();
//            propertyType.setGroupTypeId("2");
//            propertyType.setGroupType("Landed");
//            arraySubType.add(propertyType);
        }
        
        createRequest = new CreateSnatchRequest();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        if (staticResponse != null) {
            arrayCategory.remove(0);
            edtCategory.setText(arrayCategory.get(0).getCategoryName());
        }
        showProperty();
    
        System.out.println("SIZE ARRAY PROPERTY TYPES : " + arraySubType.size());
    }
    
    @OnClick(R.id.btnNext)
    void nextListener() {
        if (createRequest.getSnatchCategory() == null) {
            createRequest.setSnatchCategory(arrayCategory.get(0).getCategoryName());
            createRequest.setCategoryId(arrayCategory.get(0).getCategoryId());
        }
       if (createRequest.getWhatFor() == null) {
           createRequest.setWhatForId("1");
           createRequest.setWhatFor("Sale");
       }
    
       if ((createRequest.getSnatchPropertyGroupType() == null) && createRequest.getSnatchCategory().equalsIgnoreCase("Residential")) {
           Toast.makeText(getActivity(), "Please choose property group type", Toast.LENGTH_LONG).show();
           System.out.println("masuk validasi");
       } else if (createRequest.getSnatchCategory().equalsIgnoreCase("residential") && createRequest.getSnatchPropertyGroupType().equalsIgnoreCase("private") && createRequest.getSubType() == null) {
           Toast.makeText(getActivity(), "Please choose property group type", Toast.LENGTH_LONG).show();
       } else {
           loadReplaceFragment(R.id.container_create, CreateListingFragment
                   .newInstance(staticResponse, "For Sale", createRequest), CreateListingFragment.class.getName());
       }
       
    }
    
    @OnClick(R.id.edtCategory)
    void categoryListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayCategory.size(); i++) {
            list.add(arrayCategory.get(i).getCategoryName());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtCategory);
    }
    
    @OnClick(R.id.edtPropertyGroupType)
    void propertyGroupListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayPropertyType.size(); i++) {
            list.add(arrayPropertyType.get(i).getGroupType());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtGroupType);
    }
    
    @OnClick(R.id.edtPropertySubTypes)
    void propertysGroupListener() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arraySubType.size(); i++) {
            list.add(arraySubType.get(i).getSubType());
        }
        
        CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        showDialogList(items, edtGroupTypes);
    }
    
    @OnClick({ R.id.rbSale, R.id.rbRent }) public void onRadioButtonClicked(RadioButton radioButton) {
        // Is the button now checked?
        boolean checked = radioButton.isChecked();
        
        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.rbSale:
                if (checked) {
                    // 1 clicked
                    createRequest.setWhatForId("1");
                    createRequest.setWhatFor("Sale");
                }
                break;
            case R.id.rbRent:
                if (checked) {
                    // 2 clicked
                    createRequest.setWhatForId("2");
                    createRequest.setWhatFor("Rent");
                }
                break;
        }
    }
    
    private void showDialogList(final CharSequence[] items, final EditText editText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                if (editText.getId() == R.id.edtCategory) {
                    createRequest.setSnatchCategory(arrayCategory.get(which).getCategoryName());
                    createRequest.setCategoryId(arrayCategory.get(which).getCategoryId());
                    if (arrayCategory.get(which).getCategoryName().equalsIgnoreCase(RESIDENTIAL)) {
                        showProperty();
                    } else {
                        hideProperty();
                    }
                    
                } else if (editText.getId() == R.id.edtPropertyGroupType) {
                    createRequest.setSnatchPropertyGroupType(arrayPropertyType.get(which).getGroupType());
                    createRequest.setPropertyGroupId(arrayPropertyType.get(which).getGroupTypeId());
                    if (arrayPropertyType.get(which).getGroupType().equalsIgnoreCase("private")) {
                        tilPropertyGroupTypes.setVisibility(View.VISIBLE);
                    } else {
                        tilPropertyGroupTypes.setVisibility(View.GONE);
                    }
                } else if (editText.getId() == R.id.edtPropertySubTypes) {
                    createRequest.setSubType(arraySubType.get(which).getSubType());
                    createRequest.setSubTypeId(arraySubType.get(which).getSubTypeId());
                }
                editText.setText(items[which]);
                dialog.dismiss();
            }
        });
        builder.show();
    }
    
    private void showProperty() {
        tilPropertyGroupType.setVisibility(View.VISIBLE);
        tilPropertyGroupTypes.setVisibility(View.GONE);
    }
    
    private void hideProperty() {
        tilPropertyGroupType.setVisibility(View.GONE);
        tilPropertyGroupTypes.setVisibility(View.GONE);
    }
    
    @Subscribe
    public void onEventLocation(LocationEvent event) {
        
        if (event != null) {
           
           createRequest.setLatitude(String.valueOf(event.getCurrentLatitude()));
           createRequest.setLongitude(String.valueOf(event.getCurrentLongitude()));
           
        }
    }
}
