package com.app.snatch.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.adapter.AmenitiesAdapter;
import com.app.snatch.adapter.FacilityAdapter;
import com.app.snatch.model.response.StaticResponse;
import com.app.snatch.presenter.StaticPresenter;
import com.app.snatch.utils.Dummy;
import com.app.snatch.views.StaticView;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateAmenitiesFragment extends BaseFragment implements StaticView {
    
    @BindView(R.id.mRecycler)
    RecyclerView mRecyler;
    @BindView(R.id.txtExtraTitle)
    TextView txtTitle;
    @BindString(R.string.does_the_property_have_any_amenities)
    String amenities;
    @BindString(R.string.does_the_property_have_access_and_public_service_nearby)
    String propertyNearby;
    
    private AmenitiesAdapter amenitiesAdapter;
    private String type;
    private StaticPresenter staticPresenter;
    
    public CreateAmenitiesFragment() {
        // Required empty public constructor
    }
    
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_create_amenities;
    }
    
    public static CreateAmenitiesFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString(TYPE, type);
        CreateAmenitiesFragment fragment = new CreateAmenitiesFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setHasOptionsMenu(true);
        
        if (getArguments() != null) {
            type = getArguments().getString(TYPE);
        }
    
        staticPresenter = new StaticPresenter();
        staticPresenter.attachView(this);
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        staticPresenter.getStatic(UserPreference.getToken(getActivity()));
        
        ArrayList<String> arrayList;
        if (type.equals(AMENITIES)) {
            txtTitle.setText(amenities);
            arrayList = Dummy.arrayAmenities();
        } else {
            txtTitle.setText(propertyNearby);
            arrayList = Dummy.arrayProperty();
        }
        
        configList(mRecyler, android.R.color.transparent);
//        amenitiesAdapter = new AmenitiesAdapter(arrayList);
//        mRecyler.setAdapter(amenitiesAdapter);
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        
        if (item.getItemId() == android.R.id.home) {
            getFragmentManager().popBackStack();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public void showProgressDialog() {
    
    }
    
    @Override
    public void onStaticFail(String message) {
    
    }
    
    @Override
    public void onStaticSuccess(StaticResponse user) {
        if (type.equals(AMENITIES)) {
            txtTitle.setText(amenities);
            AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(user.getStaticData().getAmenities());
            mRecyler.setAdapter(amenitiesAdapter);
        } else {
            txtTitle.setText(propertyNearby);
            FacilityAdapter facilityAdapter = new FacilityAdapter(user.getStaticData().getFacilities());
            mRecyler.setAdapter(facilityAdapter);
        }
        
        
    }
}
