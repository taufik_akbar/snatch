package com.app.snatch.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.activity.EditProfileActivity;
import com.app.snatch.activity.LoginActivity;
import com.app.snatch.activity.NotificationActivity;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.model.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment {
    
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtDesignation)
    TextView txtDesignation;
    @BindView(R.id.txtCeano)
    TextView txtCea;
    @BindView(R.id.txtGender)
    TextView txtGender;
    @BindView(R.id.imgProfile)
    CircleImageView imgProfile;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    
    public ProfileFragment() {
        // Required empty public constructor
    }
    
    
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_profile, container, false);
//    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_profile;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    
        mToolbar.setTitle("Profile");
        
        if (!UserPreference.getName(getActivity()).isEmpty()) {
            User user = UserPreference.getUser(getActivity());
            
            txtName.setText(user.getName());
            txtCea.setText(user.getCea());
            txtDesignation.setText(user.getDesignation());
            txtGender.setText(user.getGender());
            
            String image = ApiConfig.BASE_URL_IMAGE + UserPreference.getProfPic(getActivity());
            Glide.with(getActivity()).load(image).apply(new RequestOptions().centerCrop()).into(imgProfile);
        }
    }
    
    @OnClick(R.id.btnEditProfile)
    void editListener() {
        startActivity(new Intent(getActivity(), EditProfileActivity.class));
    }
    
    @OnClick(R.id.btnSignout)
    void signoutListener() {
        UserPreference.signout(getActivity());
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }
    
    @OnClick(R.id.imgNotif)
    void notifListener() {
        startActivity(new Intent(getActivity(), NotificationActivity.class));
    }
    
}
