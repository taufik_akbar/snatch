package com.app.snatch.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.adapter.MyListingSnatchAdapter;
import com.app.snatch.model.Snatch;
import com.app.snatch.presenter.SnatchOwnedPresenter;
import com.app.snatch.views.OwnedView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyListingSnatchDetailFragment extends BaseFragment implements OwnedView {
    
    @BindView(R.id.mRecycler)
    RecyclerView mRecycler;
    @BindView(R.id.toolbar_title)
    TextView txtTitle;
    
    private String type;
    private String typeId;
    private SnatchOwnedPresenter snatchOwnedPresenter;
    
    public MyListingSnatchDetailFragment() {
        // Required empty public constructor
    }
    
    public static MyListingSnatchDetailFragment newInstance(String type, String typeId) {
        Bundle args = new Bundle();
        args.putString(TYPE, type);
        args.putString(TYPE_SNATCH, typeId);
        MyListingSnatchDetailFragment fragment = new MyListingSnatchDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_my_listing_snatch_detail;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            type = getArguments().getString(TYPE);
            typeId = getArguments().getString(TYPE_SNATCH);
        }
        
        snatchOwnedPresenter = new SnatchOwnedPresenter();
        snatchOwnedPresenter.attachView(this);
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        
        configList(mRecycler, android.R.color.transparent);
        
        txtTitle.setText(type);
        if (type.equals(OWNED_LIST)) {
            snatchOwnedPresenter.getOwned(UserPreference.getToken(getActivity()));
        } else {
            snatchOwnedPresenter.getListType(UserPreference.getToken(getActivity()), typeId, "0", "10");
        }
        
        
    }
    
    @OnClick(R.id.btnBack)
    void backListener() {
        getFragmentManager().popBackStackImmediate();
    }
    
    @Override
    public void showProgressDialog() {
    
    }
    
    @Override
    public void onOwnedFail(String message) {
    
    }
    
    @Override
    public void onOwnedSuccess(ArrayList<Snatch> snatches) {
        MyListingSnatchAdapter adapter = new MyListingSnatchAdapter(snatches, type);
        mRecycler.setAdapter(adapter);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        
        if (item.getItemId() == android.R.id.home) {
            getFragmentManager().popBackStack();
            return true;
        } else {
    
            return super.onOptionsItemSelected(item);
        }
    }
}
