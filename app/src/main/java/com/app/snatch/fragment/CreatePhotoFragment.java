package com.app.snatch.fragment;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.snatch.R;
import com.app.snatch.model.request.CreateSnatchRequest;
import com.app.snatch.model.response.StaticResponse;
import com.app.snatch.utils.ImageHelper;
import com.app.snatch.utils.PermissionHelper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreatePhotoFragment extends BaseFragment {
    
    @BindView(R.id.rlContainerPhoto)
    RelativeLayout rlContainerPhoto;
    @BindView(R.id.lnContainerPhoto)
    ViewGroup viewGroup;
    @BindView(R.id.lnBtn)
    ViewGroup lnBtn;
    @BindView(R.id.imgContainer)
    ImageView imgContainer;
    
    private int REQUEST_CAMERA = 0;
    private int REQUEST_GALLERY = 1;
    private int idContainer = 0;
    private Uri uri;
    private File fileImage;
    private File fileCompress;
    private ArrayList<String> arrayImage;
    
    private CreateSnatchRequest createSnatchRequest;
    private StaticResponse response;
    
    public CreatePhotoFragment() {
        // Required empty public constructor
    }
    
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_create_photo;
    }
    
    public static CreatePhotoFragment newInstance(CreateSnatchRequest request, StaticResponse response) {
         Bundle args = new Bundle();
        args.putSerializable(STATIC_REQUEST, request);
        args.putSerializable(STATIC_RESPONSE, response);
         CreatePhotoFragment fragment = new CreatePhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    
        arrayImage = new ArrayList<>();
        
        if (getArguments() != null) {
            createSnatchRequest = (CreateSnatchRequest) getArguments()
                    .getSerializable(STATIC_REQUEST);
    
            response = (StaticResponse) getArguments()
                    .getSerializable(STATIC_RESPONSE);
        }
    }
    
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        lnBtn.setVisibility(View.VISIBLE);
//        rlContainerPhoto.setVisibility(View.VISIBLE);
        if (ContextCompat
                .checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            takePictureButton.setEnabled(false);
            ActivityCompat
                    .requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
    }
    
    @OnClick(R.id.btnTakePhoto)
    void takePhotoListener() {
        boolean result = PermissionHelper.checkPermission(getActivity());
//        if (result) takePicture();
        if (result && arrayImage.size() < 10) {
            selectImage();
        } else {
            Toast.makeText(getActivity(), "max image is 10", Toast.LENGTH_SHORT).show();
        }
    }
    
    @OnClick(R.id.btnNext)
    void nextListener() {
        createSnatchRequest.setArrayImage(arrayImage);
        
        if (arrayImage.size() < 2) {
            Toast.makeText(getActivity(), "minimum image is 2", Toast.LENGTH_SHORT).show();
        } else {
    
            loadReplaceFragment(R.id.container_create, CreateExtraFragment.newInstance(createSnatchRequest, response), CreateExtraFragment.class
                    .getName());
        }
    }
    
    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        file = Uri.fromFile(getOutputMediaFile());
        fileImage = new File(Environment.getExternalStorageDirectory(), "test");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileImage);
        
        
        startActivityForResult(intent, 100);
    }
    
    
    private void addImage(File file, int id, int type) {
        LayoutInflater vi = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = vi.inflate(R.layout.item_photo_list, viewGroup, false);
        view.setId(id);
        ImageView imgContent = view.findViewById(R.id.imgContent);
    
        RequestOptions options = new RequestOptions();
        options.centerCrop().transform(new RoundedCorners(10));
    
        
//        if (type == REQUEST_CAMERA) {
//            Glide.with(getActivity()).load(bitmap) // Uri of the picture
//                    .apply(options)
//                    .into(imgContent);
//        } else {
            
            Glide.with(getActivity()).load(file) // Uri of the picture
                    .apply(options)
                    .into(imgContent);
//        }
        
        viewGroup.addView(view);
    }
    
    private void selectImage() {
        
        
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {
            
            @Override
            
            public void onClick(DialogInterface dialog, int item) {
                
                if (options[item].equals("Take Photo")) {
                    
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, REQUEST_CAMERA);
                    dispatchTakePictureIntent();
                    
                    
                } else if (options[item].equals("Choose from Gallery")) {
                    
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);//
                    startActivityForResult(intent, REQUEST_GALLERY);
//                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//                    startActivityForResult(intent, REQUEST_GALLERY);
                    
                    
                } else if (options[item].equals("Cancel")) {
                    
                    dialog.dismiss();
                    
                }
                
            }
            
        });
        
        builder.show();
        
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if (resultCode == RESULT_OK) {
            
            rlContainerPhoto.setVisibility(View.GONE);
            lnBtn.setVisibility(View.VISIBLE);
            if (requestCode == REQUEST_CAMERA) {
    
                try {
                    fileCompress = new Compressor(getActivity()).compressToFile(new File(uri.getPath()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                arrayImage.add(fileCompress.toString());
                addImage(fileCompress, idContainer + 1, REQUEST_CAMERA);
                
            } else if (requestCode == REQUEST_GALLERY) {
                
                Uri selectedImage = data.getData();
                String path = new File(selectedImage.getPath()).toString();
                System.out.println("PICTURE PATH : " + path);
                System.out.println("URI : " + selectedImage.getPath());
                System.out.println("URI : " + selectedImage.toString());
                
                fileImage = new File(ImageHelper.getPath(selectedImage, getActivity()));
                
//                Glide.with(getActivity()).load(fileImage).apply(new RequestOptions()
//                        .bitmapTransform(new RoundedCornersTransformation(20, 0)))
//                        .into(imgContainer);
                try {
                    fileCompress = new Compressor(getActivity()).compressToFile(fileImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                arrayImage.add(fileCompress.toString());
                addImage(fileCompress, idContainer + 1, REQUEST_GALLERY);
                
            }
        }
    }
    
    
    
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        
        switch (requestCode) {
            case PermissionHelper.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePicture();
                } else {
                    //code for deny
                }
                break;
        }
    }
    
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    
    
    
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    
    private void dispatchTakePictureIntent() {
        Calendar cal = Calendar.getInstance();
        fileImage = new File(Environment.getExternalStorageDirectory(), (cal.getTimeInMillis() + ".jpg"));
        if (!fileImage.exists()) {
            try {
                fileImage.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            fileImage.delete();
            try {
                fileImage.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        uri = Uri.fromFile(fileImage);
        Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(i, REQUEST_CAMERA);
    }
}
