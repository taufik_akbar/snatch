package com.app.snatch.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.snatch.R;
import com.app.snatch.adapter.DistrictAdapter;
import com.app.snatch.model.District;
import com.app.snatch.model.bus.DistrictEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DistrictFragment extends BaseFragment {
    
    @BindView(R.id.mRecycler)
    RecyclerView mRecycler;
    
    public DistrictFragment() {
        // Required empty public constructor
    }
    
    public static DistrictFragment newInstance(ArrayList<District> arrayDistrict) {
         Bundle args = new Bundle();
        args.putSerializable(DISTRICT, arrayDistrict);
         DistrictFragment fragment = new DistrictFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
            EventBus.getDefault().register(this);
        
        configList(mRecycler, android.R.color.transparent);
        
        ArrayList<District> arrayDistrict = (ArrayList<District>) getArguments().getSerializable(DISTRICT);
        
//        ArrayList<String> arrayData = new ArrayList<>();
//        arrayData.add("D01 - Boat Quay / Raffles Place / Marina");
//        arrayData.add("D02 - Chinatown / Tanjong Pagar");
//        arrayData.add("D03 - Alexandra / Commonwealth");
//        arrayData.add("D04 - Harbourfront / Telok Blangah");
//        arrayData.add("D05 - Buona Vista / West Coast / Clementi");
//        arrayData.add("D06 - City Hall / Clarke Quay");
//        arrayData.add("D07 - Beach Road / Bugis / Rochor");
//        arrayData.add("D08 - Farrer Park / Serangoon Rd");
//        arrayData.add("D09 - Orchard / River Valley");
//        arrayData.add("D10 - Tanglin / Holland");
//        arrayData.add("D11 - Newton / Novena");
//        arrayData.add("D12 - Balestier / Toa Payoh");
//        arrayData.add("D13 - Macpherson / Potong Pasir");
//        arrayData.add("D14 - Eunos / Geylang / Paya Lebar");
//        arrayData.add("D15 - East Coast / Marine Parade");
//        arrayData.add("D16 - Bedok / Upper East Coast");
//        arrayData.add("D17 - Changi Airport / Changi Village");
//        arrayData.add("D18 - Pasir Ris / Tampines");
//        arrayData.add("D19 - Hougang / Punggol / Sengkang");
//        arrayData.add("D20 - Ang Mo Kio / Bishan / Thomson");
//        arrayData.add("D21 - Clementi Park / Upper Bukit Timah");
//        arrayData.add("D22 - Boon Lay / Jurong / Tuas");
//        arrayData.add("D23 - Bukit Batok / Bukit Panjang / Choa Chu Kang");
//        arrayData.add("D24 - Lim Chu Kang / Tengah");
//        arrayData.add("D25 - Admiralty / Woodlands");
//        arrayData.add("D26 - Mandai / Upper Thomson");
//        arrayData.add("D27 - Sembawang / Yishun");
//        arrayData.add("D28 - Seletar / Yio Chu Kang");
        
        DistrictAdapter amenitiesAdapter = new DistrictAdapter(arrayDistrict);
        mRecycler.setAdapter(amenitiesAdapter);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_district;
    }
    
    @Subscribe
    public void onEventDistrict(DistrictEvent event) {
        if (event != null) {
            System.out.println("EVENT DISTRICT : " );
            getFragmentManager().popBackStackImmediate();
        }
    }
}
