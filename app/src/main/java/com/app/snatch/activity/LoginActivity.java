package com.app.snatch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.model.User;
import com.app.snatch.model.request.LoginRequest;
import com.app.snatch.presenter.LoginPresenter;
import com.app.snatch.views.LoginView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView, Validator.ValidationListener {
    
    @NotEmpty
    @Email
    @BindView(R.id.edtEmail)
    EditText edtMail;
    @NotEmpty
    @Password
    @BindView(R.id.edtPassword)
    EditText edtPass;
    
    private LoginPresenter loginPresenter;
    private com.mobsandgeeks.saripaar.Validator mValidator;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        loginPresenter = new LoginPresenter();
        loginPresenter.attachView(this);
    
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }
    
    @OnClick(R.id.btnSignin)
    void signinListener() {
        mValidator.validate();
//        startActivity(new Intent(mContext, HomeActivity.class));
    }
    
    @OnClick(R.id.txt_forgot)
    void forgotListener() {
    
    }
    
    @Override
    public void showProgressDialog() {
        showProgress();
    }
    
    @Override
    public void onLoginFail(String message) {
        dismissProgress();
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onLoginSuccess(User user) {
        dismissProgress();
        UserPreference.setToken(mContext, user.getToken());
        startActivity(new Intent(mContext, HomeActivity.class));
        finish();
    }
    
    
    @Override
    public void onValidationSucceeded() {
        String password = edtPass.getText().toString().trim();
        String email = edtMail.getText().toString().trim();
    
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPass(password);
        loginRequest.setEmail(email);
        loginRequest.setFcmToken("1212");
        
        loginPresenter.postLogin(loginRequest);
    }
    
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(mContext);
        
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
