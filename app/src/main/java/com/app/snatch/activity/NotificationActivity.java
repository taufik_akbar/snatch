package com.app.snatch.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.app.snatch.R;
import com.app.snatch.fragment.NotificationFragment;

import butterknife.BindView;

public class NotificationActivity extends BaseActivity {
    
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
    
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadFragment(R.id.container_notification, new NotificationFragment(), NotificationFragment.TAG);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        
        
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        
        return super.onOptionsItemSelected(item);
        
    }
}
