package com.app.snatch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.api.ApiConfig;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends BaseActivity {
    
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtCeano)
    EditText edtCea;
    @BindView(R.id.edtDesignation)
    EditText edtDesignation;
    @BindView(R.id.imgProfile)
    CircleImageView imgProfile;
    @BindView(R.id.rbGender)
    RadioGroup rbGender;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.rbFemale)
    RadioButton rbFemale;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        configProfile();
    }
    
    private void configProfile() {
        String name = UserPreference.getName(mContext);
        String ceaNo = UserPreference.getCea(mContext);
        String designation = UserPreference.getDesignation(mContext);
        String gender = UserPreference.getUserGender(mContext);
    
        System.out.println("GENDER : " + gender);
        
        edtName.setText(name);
        edtCea.setText(ceaNo);
        edtDesignation.setText(designation);
        
        if (gender.equalsIgnoreCase("male")) {
            rbMale.setChecked(true);
        } else {
            rbFemale.setChecked(true);
        }
    
        String image = ApiConfig.BASE_URL_IMAGE + UserPreference.getProfPic(mContext);
        Glide.with(mContext).load(image).apply(new RequestOptions().fitCenter()).into(imgProfile);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notif, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        
        
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_notification) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }
    
        return super.onOptionsItemSelected(item);
        
    }
}
