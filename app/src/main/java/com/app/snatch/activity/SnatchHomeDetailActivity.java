package com.app.snatch.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.fragment.CreateListingFragment;
import com.app.snatch.fragment.MyListingSnatchFragment;

import butterknife.BindView;

public class SnatchHomeDetailActivity extends BaseActivity {
    
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView txtTitle;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snatch_home_detail);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        
        setupToolbar();
        loadFragment(R.id.container_homedetail, MyListingSnatchFragment.newInstance(HOME_SNATCH),
                CreateListingFragment.TAG);
    }
    
    private void setupToolbar() {
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            String title = extra.getString(TYPE_SNATCH);
            txtTitle.setText(title);
            mToolbar.setNavigationIcon(R.drawable.ic_back);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notif, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
