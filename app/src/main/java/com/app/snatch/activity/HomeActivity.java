package com.app.snatch.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.snatch.LocationPreference;
import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.fragment.HomeFragment;
import com.app.snatch.fragment.InboxFragment;
import com.app.snatch.fragment.MyListingFragment;
import com.app.snatch.fragment.ProfileFragment;
import com.app.snatch.model.User;
import com.app.snatch.model.bus.LocationEvent;
import com.app.snatch.model.response.StaticResponse;
import com.app.snatch.service.MyLocationService;
import com.app.snatch.views.ProfileView;
import com.app.snatch.views.StaticView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindColor;
import butterknife.BindView;

public class HomeActivity extends BaseActivity implements ProfileView, StaticView {
    
    @BindView(R.id.bottomNavigation)
    BottomNavigationViewEx bottomNavigationView;
//    @BindView(R.id.btnAdd)
//    Button btnAdd;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawerLayout)
    RelativeLayout rlMain;
//    @BindView(R.id.imgProfile)
//    CircleImageView imgProfile;
//    @BindView(R.id.txtName)
//    TextView txtName;
//    @BindView(R.id.txtTime)
//    TextView txtTime;
    SystemBarTintManager tintManager;
    
    
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindColor(R.color.transparent)
    int colorTransparent;
    
//    private ProfilePresenter profilePresenter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        EventBus.getDefault().register(this);
        setSupportActionBar(mToolbar);
//        tintManager = new SystemBarTintManager(this);
//// enable status bar tint
//        tintManager.setStatusBarTintEnabled(true);
//// enable navigation bar tint
//        tintManager.setNavigationBarTintEnabled(true);
//// set the transparent color of the status bar, 20% darker
//        tintManager.setTintColor(Color.parseColor("#20000000"));
        
        bottomNavigationView.enableItemShiftingMode(false);
        bottomNavigationView.enableShiftingMode(false);
        bottomNavigationView.enableAnimation(false);
//        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        setupBottombar();
        bottomNavigationView.setSelectedItemId(R.id.action_snatch);
//        bottomNavigationView.setCurrentItem(0);
    
        configData();
        if (ActivityCompat
                .checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat
                .checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat
                    .requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 20);
        }
    
        startService(new Intent(HomeActivity.this, MyLocationService.class));
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    
    private void configData() {
//        staticPresenter = new StaticPresenter();
//        staticPresenter.attachView(this);
//        profilePresenter = new ProfilePresenter();
//        profilePresenter.attachView(this);
//        if (UserPreference.getName(mContext).isEmpty()) {
//            profilePresenter.getProfile(UserPreference.getToken(mContext));
//        } else {
//            setProfile(UserPreference.getName(mContext), UserPreference.getProfPic(mContext));
//        }
//
//        staticPresenter.getStatic(UserPreference.getToken(mContext));
    }
    
    private void setupBottombar() {
        bottomNavigationView
                .setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_snatch:
                                startFragment(getSupportFragmentManager(), new HomeFragment());
                                enableSystemTransparent();
                                mToolbar.setTitle("Snatch");
                                rlMain.setBackgroundColor(colorTransparent);
                                return true;
                            case R.id.action_inbox:
                                startFragment(getSupportFragmentManager(), new InboxFragment());
                                mToolbar.setTitle("Inbox");
                                rlMain.setBackgroundColor(colorPrimary);
//                                disableSystemTransparent();
                                return true;
                            case R.id.action_mylisting:
                                startFragment(getSupportFragmentManager(), new MyListingFragment());
                                mToolbar.setTitle("My Listing");
                                rlMain.setBackgroundColor(colorPrimary);
//                                disableSystemTransparent();
                                return true;
                            case R.id.action_profile:
                                startFragment(getSupportFragmentManager(), new ProfileFragment());
                                mToolbar.setTitle("Profile");
                                rlMain.setBackgroundColor(colorPrimary);
//                                disableSystemTransparent();
                                return true;
                        }
                        return false;
                        
                        
                    }
                });
    }
    
//    @OnClick(R.id.btnAdd)
//    void createListener() {
//        Intent intent = new Intent(HomeActivity.this, CreateListingActivity.class);
//        intent.putExtra(STATIC_RESPONSE, staticResponse);
////        startActivity(new Intent(HomeActivity.this, CreateListingActivity.class));
//        startActivity(intent);
//    }
    
    private void startFragment(FragmentManager manager, Fragment fragment) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment).commit();
    }
    
    @TargetApi(19)
    private void enableSystemTransparent() {

//        setTranslucentStatus(true);
//        Window window = getWindow();
//        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        rlProfile.setVisibility(View.VISIBLE);
//        btnAdd.setVisibility(View.VISIBLE);
//        tintManager.setStatusBarTintEnabled(true);
//        tintManager.setStatusBarAlpha(0);
    }
    
    @TargetApi(19)
    private void disableSystemTransparent() {
//        Window window = getWindow();
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        
//        rlProfile.setVisibility(View.GONE);
//        btnAdd.setVisibility(View.GONE);
//        setTranslucentStatus(false);
//        tintManager.setStatusBarTintEnabled(false);
//        tintManager.setTintColor(colorPrimary);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        }
    }
    
    @TargetApi(19)
    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }
    
    @Override
    public void showProgressDialog() {
    
    }
    
    @Override
    public void onProfileFail(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onProfileSuccess(User user) {
        UserPreference.setUser(mContext, user);
        setProfile(user.getName(), user.getImagePath().getImage());
        
        
    }
    
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 20:
                if (grantResults.length > 0) {
                    startService(new Intent(HomeActivity.this, MyLocationService.class));
                } else {
                    ActivityCompat
                            .requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 20);
                }
                return;
        }
    }
    
    private void setProfile(String name, String image) {
//        txtName.setText(name);
        String urlImage = ApiConfig.BASE_URL_IMAGE + image;
        System.out.println("URL IMAGE : " + urlImage);
//        Glide.with(mContext).load(urlImage).apply(new RequestOptions().fitCenter()).into(imgProfile);
        
        Format formatter = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        String today = formatter.format(new Date());
//        txtTime.setText(today);
        System.out.println("Today : " + today);
    }
    
    @Override
    public void onStaticFail(String message) {
    
    }
    
    @Override
    public void onStaticSuccess(StaticResponse user) {
//        this.staticResponse = user;
//        EventBus.getDefault().post(new StaticEvent(user));
    }
    
    @Subscribe
    public void onEventLocation(LocationEvent event) {
        
        if (event != null) {
            LocationPreference.setLocation(mContext, String.valueOf(event.getCurrentLatitude()), String.valueOf(event.getCurrentLongitude()));
            
            
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
