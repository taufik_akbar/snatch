package com.app.snatch.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.app.snatch.utils.Constants;
import com.app.snatch.utils.DividerItemDecoration;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by Taufik Akbar on 21/12/2016.
 */

public class BaseActivity extends AppCompatActivity implements Constants {

    protected Context mContext;
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }
    
    protected void configList(RecyclerView mRecycler, int lineColor) {
        mRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(mContext);
        mRecycler.setLayoutManager(manager);
        mRecycler.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL, lineColor));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
    }
    
    protected void configGrid(RecyclerView mRecycler, int lineColor) {
        mRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(mContext);
        mRecycler.setLayoutManager(manager);
        mRecycler.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL, lineColor));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
    }
    
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected void setupToolbar(Toolbar mToolbar) {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
//            mToolbar.setNavigationIcon(R.drawable.ic_back);
        }
    }

    protected void setupToolbarWithTitle(Toolbar mToolbar, TextView textView, String title) {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mToolbar.setTitle("");
            getSupportActionBar().setTitle("");
//            mToolbar.setNavigationIcon(R.drawable.ic_back);
            textView.setText(title);
        }
    }

    protected void loadFragment(int container, Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .add(container, fragment, tag)
                .addToBackStack(tag).commit();
    }
    
    protected void showProgress() {
        progressDialog = new ProgressDialog(mContext);
//        progressDialog.setMessage(getResources().getString(R.string.prog_dialog_send));
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.setCancelable(true);
        progressDialog.show();
    }
    
    protected void dismissProgress() {
        progressDialog.dismiss();
    }
    
    
    @Override
    public void onBackPressed() {
        int fragmentCount = getSupportFragmentManager().getBackStackEntryCount();

        if (fragmentCount > 1) {
            getSupportFragmentManager().popBackStack();
        } else if (fragmentCount == 1){
            finish();
        } else {
            super.onBackPressed();
        }
    }

}
