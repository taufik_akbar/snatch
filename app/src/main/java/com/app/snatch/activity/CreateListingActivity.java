package com.app.snatch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.app.snatch.R;
import com.app.snatch.fragment.CreateListingCategoryFragment;
import com.app.snatch.model.response.StaticResponse;

import butterknife.BindView;

public class CreateListingActivity extends BaseActivity {
    
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_listing);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        mToolbar.setTitle("Edit Profile");
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        
        Bundle bundle = getIntent().getExtras();
        StaticResponse response = (StaticResponse) bundle.getSerializable(STATIC_RESPONSE);
        CreateListingCategoryFragment fragment = new CreateListingCategoryFragment();
//        fragment.setArguments(bundle);
        loadFragment(R.id.container_create, CreateListingCategoryFragment.newInstance(response), CreateListingCategoryFragment.class.getSimpleName());
    
        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notif, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        
        
        if (item.getItemId() == android.R.id.home) {
            int fragmentCount = getSupportFragmentManager().getBackStackEntryCount();
    
            if (fragmentCount > 1) {
                getSupportFragmentManager().popBackStack();
            } else if (fragmentCount == 1){
                finish();
            }
            return true;
        } else if (item.getItemId() == R.id.action_notification) {
            startActivity(new Intent(mContext, NotificationActivity.class));
            return true;
        } else {
    
            return super.onOptionsItemSelected(item);
        }
        
        
    }
}
