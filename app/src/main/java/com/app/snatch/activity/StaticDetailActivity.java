package com.app.snatch.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.adapter.StaticAdapter;
import com.app.snatch.model.StaticDetail;

import java.util.ArrayList;

import butterknife.BindView;

public class StaticDetailActivity extends BaseActivity {
    
    @BindView(R.id.mRecycler)
    RecyclerView mRecycler;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView txtToolbarTitle;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_detail);
        getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        configToolbar();
        Bundle extra = getIntent().getExtras();
        String type = extra.getString(TYPE);
        txtToolbarTitle.setText(extra.getString(TITLE));
        
        if (type.equals(AMENITIES)) {
            txtTitle.setText("Amenities");
        } else {
            txtTitle.setText("Commute & Nearby Places");
        }
        ArrayList<StaticDetail> arrayList = (ArrayList<StaticDetail>) getIntent()
                .getSerializableExtra(SNATCH_DATA);
        configGrid(arrayList, type);
    }
    
    private void configToolbar() {
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
    }
    
    private void configGrid(ArrayList<StaticDetail> arrayList, String type) {
        mRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new GridLayoutManager(mContext, 4);
        mRecycler.setLayoutManager(manager);
        
        StaticAdapter adapter = new StaticAdapter(arrayList, type, "");
        mRecycler.setAdapter(adapter);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        
        
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        
        return super.onOptionsItemSelected(item);
        
    }
}
