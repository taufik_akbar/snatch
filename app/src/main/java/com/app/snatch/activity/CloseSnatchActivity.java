package com.app.snatch.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.snatch.R;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

public class CloseSnatchActivity extends BaseActivity {
    
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.btnSave)
    Button btnCancel;
    @BindView(R.id.btnNext)
    Button btnClose;
    
    @BindString(R.string.close_snatch)
    String closeSnatch;
    @BindString(R.string.cancel)
    String cancelSnatch;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_snatch);
        getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setupToolbar();
    
        btnCancel.setText(cancelSnatch);
        btnClose.setText(closeSnatch);
    }
    
    private void setupToolbar() {
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
    }
    
    @OnClick(R.id.btnSave)
    void cancelListener() {
        finish();
    }
    
    @OnClick(R.id.btnNext)
    void closeListener() {
        finish();
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
