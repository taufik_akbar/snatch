package com.app.snatch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.app.snatch.R;
import com.app.snatch.UserPreference;

public class SplashActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        
        new Handler().postDelayed(new Runnable() {
 
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
        
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
            
                Intent intent;
                if (!UserPreference.getToken(getApplicationContext()).isEmpty()) {
                
                    intent = new Intent(SplashActivity.this, HomeActivity.class);
                
                } else {

                    intent = new Intent(SplashActivity.this, LoginActivity.class);

                }
            
                startActivity(intent);
            
                // close this activity
                finish();
            }
        }, 2000);
    }
}
