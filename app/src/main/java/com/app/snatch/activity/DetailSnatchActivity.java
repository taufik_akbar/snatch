package com.app.snatch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.adapter.ImagePagerAdapter;
import com.app.snatch.adapter.SnatcherAdapter;
import com.app.snatch.adapter.StaticAdapter;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.model.AddSnatchRequest;
import com.app.snatch.model.ImagePath;
import com.app.snatch.model.Snatch;
import com.app.snatch.model.SnatchDetail;
import com.app.snatch.model.StaticDetail;
import com.app.snatch.model.User;
import com.app.snatch.presenter.AddSnatchPresenter;
import com.app.snatch.presenter.DetailSnatchPresenter;
import com.app.snatch.views.DefaultView;
import com.app.snatch.views.DetailSnatchView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;

public class DetailSnatchActivity extends BaseActivity implements DetailSnatchView, DefaultView {
    
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.mPager)
    ViewPager mPager;
    @BindView(R.id.mRecyclerSnatcher)
    RecyclerView mRecyclerSnatcher;
    @BindView(R.id.mRecyclerNearby)
    RecyclerView mRecylerNearby;
    @BindView(R.id.mRecyclerAmenities)
    RecyclerView mRecylerAmenities;
    @BindView(R.id.mIndicator)
    CircleIndicator mIndicator;
    @BindView(R.id.imgProfile)
    CircleImageView imgProfile;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtCategory)
    TextView txtCategory;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtRoom)
    TextView txtRoom;
    @BindView(R.id.txtArea)
    TextView txtArea;
    @BindView(R.id.txtAgent)
    TextView txtAgent;
    @BindView(R.id.txtFurnished)
    TextView txtFurnished;
    @BindView(R.id.txtFloor)
    TextView txtFloor;
    @BindView(R.id.txtAvailable)
    TextView txtAvailable;
    @BindView(R.id.txtDesc)
    TextView txtDesc;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.txtTenure)
    TextView txtTenure;
    @BindView(R.id.btnSnatch)
    Button btnSnatch;
    @BindColor(R.color.colorGrey3)
    int colorDisable;
    @BindColor(R.color.colorCloseSnatch)
    int colorClose;
    @BindColor(R.color.colorPrimary)
    int colorEnable;
    
    private DetailSnatchPresenter detailPresenter;
    private AddSnatchPresenter addSnatchPresenter;
    private Snatch snatchData;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_snatch);
        
        getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        configToolbar();
        getDataDetail();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        addSnatchPresenter.detachView();
        detailPresenter.detachView();
    }
    
    private void getDataDetail() {
        detailPresenter = new DetailSnatchPresenter();
        addSnatchPresenter = new AddSnatchPresenter();
        detailPresenter.attachView(this);
        addSnatchPresenter.attachView(this);
        
        Bundle extra = getIntent().getExtras();
        snatchData = (Snatch) getIntent().getSerializableExtra(SNATCH_DATA);
        if (extra != null) {
            detailPresenter.getDetail(UserPreference.getToken(mContext), snatchData.getSnatchId());
            txtTitle.setText(snatchData.getSnatchName());
            txtCategory.setText(snatchData.getSnatchType());
            
            
        }
    }
    
    private void configToolbar() {
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    private void configPager(ArrayList<ImagePath> arrayImage) {
        ImagePagerAdapter adapter = new ImagePagerAdapter(mContext, arrayImage);
        mPager.setAdapter(adapter);
        mIndicator.setViewPager(mPager);
    }
    
    private void configSnatcher(ArrayList<User> snatcher) {
        configList(mRecyclerSnatcher, android.R.color.transparent);
        SnatcherAdapter adapter = new SnatcherAdapter(snatcher, getSupportFragmentManager());
        mRecyclerSnatcher.setAdapter(adapter);
    }
    
    private void configStatic(ArrayList<StaticDetail> snatcher, RecyclerView mRecycler, String type, String title) {
        mRecycler.setHasFixedSize(true);
        mRecycler
                .setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        StaticAdapter adapter = new StaticAdapter(snatcher, type, title);
        mRecycler.setAdapter(adapter);
        
    }
    
    @OnClick(R.id.btnSnatch)
    void snatchListner() {
        if (btnSnatch.getText().toString().equalsIgnoreCase("Close")) {
            startActivity(new Intent(DetailSnatchActivity.this, CloseSnatchActivity.class));
        } else {
    
            AddSnatchRequest request = new AddSnatchRequest();
            request.setSnatchId(snatchData.getSnatchId());
            addSnatchPresenter.addSnatch(UserPreference.getToken(mContext), request);
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_more, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void showProgressDialog() {
    
    }
    
    @Override
    public void onDetailFail(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onDetailSuccess(SnatchDetail snatch) {
        
        String room = snatch.getBedroom() + " Bedroom";
        String measurement = snatch.getFloorArea() + " feet";
        String agentName = snatch.getUser().getName();
        String address = snatch.getLocationBuilding() + ", Block " + snatch
                .getLocationBlock() + ", " + snatch.getLocationRoad() + ", " + snatch
                .getLocationPostal();
        txtRoom.setText(room);
        txtArea.setText(measurement);
        txtDesc.setText(snatch.getDescription());
        txtAddress.setText(address);
        txtAgent.setText(agentName);
//        txtFurnished.setText(snatch.get);
        
        Locale localeID = new Locale("zh", "SG");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String price = "SG" + formatRupiah.format(Double.parseDouble(snatch.getSnatchPrice()));
        txtPrice.setText(price);
        
        String tenure = "Tenure " + snatch.getTenure();
        txtTenure.setText(tenure);
        
        String profPic = ApiConfig.BASE_URL_IMAGE + snatch.getUser().getImagePath().getImage();
        Glide.with(mContext).load(profPic).apply(new RequestOptions().centerCrop())
                .into(imgProfile);
        
        configPager(snatch.getSnatchImage());
        configSnatcher(snatch.getSnatcherList());
        
        ArrayList<StaticDetail> arrayNearby = snatch.getFacilities();
        for (int i = 0; i < arrayNearby.size(); i++) {
            
            String id = arrayNearby.get(i).getId();
            
            switch (id) {
                case "1":
                    arrayNearby.get(i).setImage(R.drawable.ic_commute);
                    break;
                case "2":
                    arrayNearby.get(i).setImage(R.drawable.ic_mrt_stations);
                    break;
                case "3":
                    arrayNearby.get(i).setImage(R.drawable.ic_school);
                    break;
                case "4":
                    arrayNearby.get(i).setImage(R.drawable.ic_post_office);
                    break;
                case "5":
                    arrayNearby.get(i).setImage(R.drawable.ic_atm);
                    break;
                case "6":
                    arrayNearby.get(i).setImage(R.drawable.ic_supermarket);
                    break;
                case "7":
                    arrayNearby.get(i).setImage(R.drawable.ic_clinic);
                    break;
                case "8":
                    arrayNearby.get(i).setImage(R.drawable.ic_bus_stop);
                    break;
                case "9":
                    arrayNearby.get(i).setImage(R.drawable.ic_bank);
                    break;
                case "10":
                    arrayNearby.get(i).setImage(R.drawable.ic_town_park);
                    break;
                
            }
        }
    
        ArrayList<StaticDetail> arrayAmenities = snatch.getAmenities();
        for (int i = 0; i < arrayAmenities.size(); i++) {
        
            String id = arrayAmenities.get(i).getId();
        
            switch (id) {
                case "1":
                    arrayAmenities.get(i).setImage(R.drawable.ic_swimming_pool);
                    break;
                case "2":
                    arrayAmenities.get(i).setImage(R.drawable.ic_basement_parking);
                    break;
                case "3":
                    arrayAmenities.get(i).setImage(R.drawable.ic_parking);
                    break;
                case "4":
                    arrayAmenities.get(i).setImage(R.drawable.ic_cctv_security);
                    break;
                case "5":
                    arrayAmenities.get(i).setImage(R.drawable.ic_gym);
                    break;
                case "6":
                    arrayAmenities.get(i).setImage(R.drawable.ic_fridge);
                    break;
                case "7":
                    arrayAmenities.get(i).setImage(R.drawable.ic_jacuzzi);
                    break;
                case "8":
                    arrayAmenities.get(i).setImage(R.drawable.ic_closet);
                    break;
                case "9":
                    arrayAmenities.get(i).setImage(R.drawable.ic_balcony);
                    break;
                case "10":
                    arrayAmenities.get(i).setImage(R.drawable.ic_bbq_pits);
                    break;
                case "11":
                    arrayAmenities.get(i).setImage(R.drawable.ic_stove);
                    break;
                case "12":
                    arrayAmenities.get(i).setImage(R.drawable.ic_air_conditioner);
                    break;
                case "13":
                    arrayAmenities.get(i).setImage(R.drawable.ic_washer);
                    break;
                case "14":
                    arrayAmenities.get(i).setImage(R.drawable.ic_dryer);
                    break;
                case "15":
                    arrayAmenities.get(i).setImage(R.drawable.ic_playground);
                    break;
            
            }
        }
        
        if (snatch.isSnatched()) {
            btnSnatch.setText("SNATCHED");
            btnSnatch.setBackgroundColor(colorDisable);
            btnSnatch.setEnabled(false);
            btnSnatch.setClickable(false);
        } else {
            btnSnatch.setText("SNATCH");
            btnSnatch.setBackgroundColor(colorEnable);
            btnSnatch.setEnabled(true);
            btnSnatch.setClickable(true);
        }
        
        if (snatch.getSnatcherList().size() > 10) {
            btnSnatch.setText("UNVAILABLE");
            btnSnatch.setBackgroundColor(colorDisable);
            btnSnatch.setEnabled(false);
            btnSnatch.setClickable(false);
        }
    
        if (snatch.getUser().getUserId().equals(UserPreference.getUserId(mContext))) {
            btnSnatch.setEnabled(true);
            btnSnatch.setBackgroundColor(colorClose);
            btnSnatch.setText("CLOSE");
        }
        
        configStatic(arrayAmenities, mRecylerAmenities, VIEW_AMENITIES, snatchData.getSnatchName());
        configStatic(arrayNearby, mRecylerNearby, VIEW_NEARBY, snatchData.getSnatchName());
    }
    
    @Override
    public void onDefaultFail(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onDefaultSuccess(String message) {
        Toast.makeText(mContext, "SUCCESS! YOU SNATCH THIS PROPERTY", Toast.LENGTH_SHORT).show();
        btnSnatch.setText("SNATCHED");
        btnSnatch.setBackgroundColor(colorDisable);
        btnSnatch.setEnabled(false);
        btnSnatch.setClickable(false);
    }
}
