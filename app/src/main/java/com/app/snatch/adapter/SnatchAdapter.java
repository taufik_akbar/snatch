package com.app.snatch.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.UserPreference;
import com.app.snatch.fragment.MyListingSnatchDetailFragment;
import com.app.snatch.fragment.MyListingSnatchFragment;
import com.app.snatch.model.Category;
import com.app.snatch.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class SnatchAdapter extends RecyclerView.Adapter<SnatchAdapter.SnatchHolder>
        implements Constants {
    
    private ArrayList<Category> categories;
    private Context mContext;
    private FragmentManager fragmentManager;
    
    public SnatchAdapter(ArrayList<Category> schoolClasses, FragmentManager fragmentManager) {
        
        this.fragmentManager = fragmentManager;
    
        Category category = new Category();
        category.setCategoryId("3");
        category.setCategoryName(LATEST_LISTING);
////        user.getStaticData().getCategories().add(category);
//        arrayCategory = user.getStaticData().getCategories();
        schoolClasses.add(0, category);
        this.categories = schoolClasses;
    }
    
    @Override
    public SnatchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_snatch, parent, false);
        return new SnatchHolder(view);
    }
    
    @Override
    public void onBindViewHolder(SnatchHolder holder, int position) {
        
        Category data = categories.get(position);
        String type = "";
        if (data.getCategoryName().equalsIgnoreCase("Commercial")){
            holder.txtName.setText(categories.get(position).getCategoryName());
            type = "1";
        } else if (data.getCategoryName().equalsIgnoreCase("Residential")){
            holder.txtName.setText(categories.get(position).getCategoryName());
            type = "2";
        } else {
            holder.txtName.setText(LATEST_LISTING);
            type = "0";
        }
        
        holder.mRecycler.setHasFixedSize(true);
        holder.mRecycler
                .setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        
        SnatchHorizontalAdapter adapter = new SnatchHorizontalAdapter(type, UserPreference.getToken(mContext));
        holder.mRecycler.setAdapter(adapter);
        
        holder.rlSeeall.setOnClickListener(new ItemListener(position));
    }
    
    @Override
    public int getItemCount() {
        return categories.size();
    }
    
    private class ItemListener implements View.OnClickListener {
        
        private String type;
        private String typeId;
        
        ItemListener(int pos) {
            if (categories.get(pos).getCategoryName().equalsIgnoreCase("Commercial")) {
                type = COMMERCIAL;
                typeId = "1";
            } else if (categories.get(pos).getCategoryName().equalsIgnoreCase("Residential")) {
                type = RESIDENTIAL;
                typeId = "2";
            } else {
                type = LATEST_LISTING;
                typeId = "0";
            }
        }
        
        @Override
        public void onClick(View v) {
    
            fragmentManager.beginTransaction()
                    .add(R.id.container, MyListingSnatchDetailFragment.newInstance(type, typeId),
                            MyListingSnatchFragment.class.getSimpleName())
                    .addToBackStack(MyListingSnatchFragment.class.getSimpleName()).commit();
//            Intent intent = new Intent(mContext, SnatchHomeDetailActivity.class);
//            intent.putExtra(TYPE_SNATCH, type);
//            mContext.startActivity(intent);
        }
    }
//
//    public void setData(ArrayList<Article> categories) {
//        this.categories = categories;
//        notifyDataSetChanged();
//    }
    
    static class SnatchHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mRecycler)
        RecyclerView mRecycler;
        @BindView(R.id.txtCategory)
        TextView txtName;
        @BindView(R.id.rlSeeall)
        RelativeLayout rlSeeall;
//        TextView txtTitle;
//        @BindView(R.id.txt_desc)
//        TextView txtDesc;
//        @BindView(R.id.img_content)
//        ImageView imgContent;
        
        
        SnatchHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
