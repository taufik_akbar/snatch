package com.app.snatch.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.activity.CloseSnatchActivity;
import com.app.snatch.activity.DetailSnatchActivity;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.model.Snatch;
import com.app.snatch.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class MyListingSnatchAdapter extends RecyclerView.Adapter<MyListingSnatchAdapter.ArticleHolder> implements Filterable, Constants {
    
    private ArrayList<Snatch> snatchArrayList;
    private Context mContext;
    private String type;
    
    public MyListingSnatchAdapter(ArrayList<Snatch> schoolClasses, String type) {
        this.snatchArrayList = schoolClasses;
        this.type = type;
    }
    
    
    @Override
    public ArticleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mylisting_snatch, parent, false);
        return new ArticleHolder(view);
    }
    
    @Override
    public void onBindViewHolder(ArticleHolder holder, int position) {
        
        Snatch data = snatchArrayList.get(position);
        
        if (type.equals(MY_SNATCH)) {
            holder.btnSnatch.setText("Snatch Closed");
            holder.btnSnatch.setBackground(holder.bgSnatchClose);
            holder.btnSnatch.setEnabled(false);
            holder.btnSnatch.setFocusable(false);
            holder.btnSnatch.setClickable(false);
        } else if (type.equals(OWNED_LIST)) {
            holder.btnSnatch.setText("Close Snatch");
            holder.btnSnatch.setBackground(holder.bgCloseSnatch);
        } else {
            holder.btnSnatch.setVisibility(View.GONE);
        }
    
        String profPic = ApiConfig.BASE_URL_IMAGE + data.getUser().getImagePath().getImage();
        String contentPic = ApiConfig.BASE_URL_IMAGE + data.getImagePath().getImage();
    
        Locale localeID = new Locale("zh", "SG");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String price = "SG" + formatRupiah.format(Double.parseDouble(data.getSnatchPrice()));
    
        holder.txtName.setText(data.getUser().getName());
        holder.txtCategory.setText(data.getSnatchType());
        holder.txtPrice.setText(price);
        holder.txtTitle.setText(data.getSnatchName());
        Glide.with(mContext).load(profPic).apply(new RequestOptions().centerCrop()).into(holder.imgProfile);
        Glide.with(mContext).load(contentPic).apply(new RequestOptions().centerCrop()).into(holder.imgContent);
        
        holder.itemView.setOnClickListener(new ItemListener(position));
        holder.btnSnatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CloseSnatchActivity.class);
//                intent.putExtra(TYPE_SNATCH, type);
                mContext.startActivity(intent);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return snatchArrayList.size();
    }
    
    @Override
    public Filter getFilter() {
        return null;
    }
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        
        ItemListener(int pos) {
            this.positionClick = pos;
        }
        
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(SNATCH_DATA, snatchArrayList.get(positionClick));
            Intent intent = new Intent(mContext, DetailSnatchActivity.class);
            intent.putExtras(bundle);
            mContext.startActivity(intent);
        }
    }
//
//    public void setData(ArrayList<Article> snatchArrayList) {
//        this.snatchArrayList = snatchArrayList;
//        notifyDataSetChanged();
//    }
    
    static class ArticleHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtCategory)
        TextView txtCategory;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.imgContent)
        ImageView imgContent;
        @BindView(R.id.imgProfile)
        CircleImageView imgProfile;
        @BindView(R.id.btnSnatch)
        Button btnSnatch;
        
        @BindDrawable(R.drawable.bg_btn_snatch_closed)
        Drawable bgSnatchClose;
        @BindDrawable(R.drawable.bg_btn_closed_snatch)
        Drawable bgCloseSnatch;
        
        ArticleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
