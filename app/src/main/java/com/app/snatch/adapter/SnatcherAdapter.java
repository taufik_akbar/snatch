package com.app.snatch.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.model.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class SnatcherAdapter extends RecyclerView.Adapter<SnatcherAdapter.AmenitiesHolder> implements Filterable {
    
    private ArrayList<User> articleArrayList;
    private Context mContext;
    private FragmentManager fragmentManager;
    
    public SnatcherAdapter(ArrayList<User> schoolClasses, FragmentManager fragmentManager) {
        this.articleArrayList = schoolClasses;
        this.fragmentManager = fragmentManager;
    }
    
    
    @Override
    public AmenitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_snatcher, parent, false);
        return new AmenitiesHolder(view);
    }
    
    @Override
    public void onBindViewHolder(AmenitiesHolder holder, int position) {
        User data = articleArrayList.get(position);
        String pathImage =  ApiConfig.BASE_URL_IMAGE + data.getImagePath().getImage();
        Glide.with(mContext).load(pathImage) // Uri of the picture
                .apply(new RequestOptions().centerCrop()).into(holder.imgContent);

        holder.txtTitle.setText(data.getName());
//        String title = articleArrayList.get(position);
//
//        holder.txtTitle.setText(title);
    }
    
    @Override
    public int getItemCount() {
        return articleArrayList.size();
    }
    
    @Override
    public Filter getFilter() {
        return null;
    }
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        
        ItemListener(int pos) {
            this.positionClick = pos;
        }
        
        @Override
        public void onClick(View v) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container_article, ArticleDetailFragment.newInstance(articleArrayList.get(positionClick)), ArticleDetailFragment.TAG_ARTICLE_DETAIL)
//                    .commit();
        }
    }
//
//    public void setData(ArrayList<Article> articleArrayList) {
//        this.articleArrayList = articleArrayList;
//        notifyDataSetChanged();
//    }
    
    static class AmenitiesHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtProfile)
        TextView txtTitle;
        //        @BindView(R.id.txt_desc)
//        TextView txtDesc;
        @BindView(R.id.imgProfile)
        CircleImageView imgContent;
        
        
        AmenitiesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
