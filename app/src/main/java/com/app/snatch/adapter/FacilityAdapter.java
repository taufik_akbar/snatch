package com.app.snatch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.model.NearbyPlace;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class FacilityAdapter extends RecyclerView.Adapter<FacilityAdapter.AmenitiesHolder> implements Filterable {
    
    private ArrayList<NearbyPlace> nearbyPlaceArrayList;
    private Context mContext;
    
    public FacilityAdapter(ArrayList<NearbyPlace> schoolClasses) {
        this.nearbyPlaceArrayList = schoolClasses;
    }
    
    
    @Override
    public AmenitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_amenities, parent, false);
        return new AmenitiesHolder(view);
    }
    
    @Override
    public void onBindViewHolder(final AmenitiesHolder holder, final int position) {
    
        NearbyPlace data = nearbyPlaceArrayList.get(position);
        
        holder.txtTitle.setText(data.getNearbyPlace());
        
//        holder.mCheckbox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                holder.mCheckbox.setTag(String.valueOf(position));
//
//                if (holder.mCheckbox.getTag().equals(position) && holder.mCheckbox.isChecked()) {
//                    holder.mCheckbox.setChecked(false);
//                } else {
//                    holder.mCheckbox.setChecked(true);
//                }
//            }
//        });
        holder.itemView.setOnClickListener(new ItemListener(position, holder.imgCheck));
    }
    
    @Override
    public int getItemCount() {
        return nearbyPlaceArrayList.size();
    }
    
    @Override
    public Filter getFilter() {
        return null;
    }
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        private ImageView mCheckbox;
        
        ItemListener(int pos, ImageView mCheckbox) {
            this.positionClick = pos;
            this.mCheckbox = mCheckbox;
        }
        
        @Override
        public void onClick(View v) {
            System.out.println("AMENITIES CHECK : " + nearbyPlaceArrayList.get(positionClick).isSelected());
            if (!nearbyPlaceArrayList.get(positionClick).isSelected()) {
        
                System.out.println("MASUK GONE");
                nearbyPlaceArrayList.get(positionClick).setSelected(true);
                mCheckbox.setVisibility(View.VISIBLE);
            } else {
                System.out.println("MASUK VISIBLE");
                nearbyPlaceArrayList.get(positionClick).setSelected(false);
                mCheckbox.setVisibility(View.GONE);
            }
        }
    }
//
//    public void setData(ArrayList<Article> nearbyPlaceArrayList) {
//        this.nearbyPlaceArrayList = nearbyPlaceArrayList;
//        notifyDataSetChanged();
//    }
    
    static class AmenitiesHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.mCheckbox)
        CheckBox mCheckbox;
//        @BindView(R.id.txt_desc)
//        TextView txtDesc;
        @BindView(R.id.imgCheck)
        ImageView imgCheck;
        
        
        AmenitiesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
