package com.app.snatch.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.activity.DetailSnatchActivity;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.model.Snatch;
import com.app.snatch.presenter.SnatchListPresenter;
import com.app.snatch.utils.Constants;
import com.app.snatch.views.OwnedView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class SnatchHorizontalAdapter extends RecyclerView.Adapter<SnatchHorizontalAdapter.SnatchHorizontalHolder> implements OwnedView, Constants {
    
    private ArrayList<Snatch> categories;
    private Context mContext;
    private SnatchListPresenter presenter;
    
    public SnatchHorizontalAdapter(String type, String token) {
        presenter = new SnatchListPresenter();
        presenter.attachView(this);
        
        presenter.getCategoryList(token, type, "0", "5");
    }
    
    
    @Override
    public SnatchHorizontalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_snatch_horizontal, parent, false);
        return new SnatchHorizontalHolder(view);
    }
    
    @Override
    public void onBindViewHolder(SnatchHorizontalHolder holder, int position) {
        
        Snatch data = categories.get(position);
        
        if (data != null) {
            holder.txtAddress.setText(data.getSnatchName());
            holder.txtCategory.setText(data.getSnatchType());
    
            Locale localeID = new Locale("zh", "SG");
            NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
            String price = "SG" + formatRupiah.format(Double.parseDouble(data.getSnatchPrice()));
            holder.txtPrice.setText(price);
    
            String contentPic = ApiConfig.BASE_URL_IMAGE + data.getImagePath().getImage();
            Glide.with(mContext).load(contentPic).apply(new RequestOptions().centerCrop()).into(holder.imgContent);
            holder.rlHorizontal.setOnClickListener(new ItemListener(position));
        }
    }
    
    @Override
    public int getItemCount() {
        if (categories != null) {
            
            return categories.size();
        } else {
            return 0;
        }
    }
    
    @Override
    public void showProgressDialog() {
    
    }
    
    @Override
    public void onOwnedFail(String message) {
    
    }
    
    @Override
    public void onOwnedSuccess(ArrayList<Snatch> snatches) {
        categories = snatches;
        notifyDataSetChanged();
    }
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        
        ItemListener(int pos) {
            this.positionClick = pos;
        }
        
        @Override
        public void onClick(View v) {
//            Intent intent = new Intent(mContext, DetailSnatchActivity.class);
//            intent.putExtra(SNATCH_DATA, categories.get(positionClick));
//            mContext.startActivity(new Intent(mContext, DetailSnatchActivity.class));
    
//            Bundle bundle = new Bundle();
//            bundle.putSerializable(SNATCH_DATA, categories.get(positionClick));
            Intent intent = new Intent(mContext, DetailSnatchActivity.class);
            intent.putExtra(SNATCH_DATA, categories.get(positionClick));
            mContext.startActivity(intent);
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container_article, ArticleDetailFragment.newInstance(articleArrayList.get(positionClick)), ArticleDetailFragment.TAG_ARTICLE_DETAIL)
//                    .commit();
        }
    }
//
//    public void setData(ArrayList<Article> articleArrayList) {
//        this.articleArrayList = articleArrayList;
//        notifyDataSetChanged();
//    }
    
    static class SnatchHorizontalHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rlHorizontal)
        ConstraintLayout rlHorizontal;
        @BindView(R.id.txtAddress)
        TextView txtAddress;
        @BindView(R.id.txtCategory)
        TextView txtCategory;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        //        @BindView(R.id.txt_desc)
//        TextView txtDesc;
        @BindView(R.id.imgContent)
        ImageView imgContent;
        
        
        SnatchHorizontalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
