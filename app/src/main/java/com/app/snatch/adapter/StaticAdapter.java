package com.app.snatch.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.activity.StaticDetailActivity;
import com.app.snatch.model.StaticDetail;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.snatch.utils.Constants.AMENITIES;
import static com.app.snatch.utils.Constants.PROPERTY_NEARBY;
import static com.app.snatch.utils.Constants.STATIC_DATA;
import static com.app.snatch.utils.Constants.TITLE;
import static com.app.snatch.utils.Constants.TYPE;
import static com.app.snatch.utils.Constants.VIEW_AMENITIES;
import static com.app.snatch.utils.Constants.VIEW_NEARBY;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class StaticAdapter extends RecyclerView.Adapter<StaticAdapter.AmenitiesHolder> implements Filterable {
    
    private ArrayList<StaticDetail> articleArrayList;
    private Context mContext;
    private String type;
    private String title;
    
    public StaticAdapter(ArrayList<StaticDetail> schoolClasses, String type, String title) {
        this.articleArrayList = schoolClasses;
        this.type = type;
        this.title = title;
    }
    
    
    @Override
    public AmenitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_nearby, parent, false);
        return new AmenitiesHolder(view);
    }
    
    @Override
    public void onBindViewHolder(AmenitiesHolder holder, int position) {
        StaticDetail data = articleArrayList.get(position);
        
        holder.imgContent.setImageResource(data.getImage());
        holder.txtTitle.setText(data.getValue());
        
        if (type.equals(VIEW_AMENITIES) || type.equals(VIEW_NEARBY)) {
            if (position == 3) {
                holder.imgContent.setVisibility(View.GONE);
                holder.txtTitle.setText("+ " + (articleArrayList.size() - 3));
                holder.txtTitle.setTextSize(20);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, StaticDetailActivity.class);
                        intent.putExtra(STATIC_DATA, articleArrayList);
                        intent.putExtra(TITLE, title);
                        if (type.equals(VIEW_AMENITIES)) {
                            intent.putExtra(TYPE, AMENITIES);
                        } else {
                            intent.putExtra(TYPE, PROPERTY_NEARBY);
                        }
                        mContext.startActivity(intent);
                    }
                });
            }
        }
    }
    
    @Override
    public int getItemCount() {
        if (type.equals(AMENITIES) || type.equals(PROPERTY_NEARBY)) {
    
            return articleArrayList.size();
        } else {
            return 4;
        }
    }
    
    @Override
    public Filter getFilter() {
        return null;
    }
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        
        ItemListener(int pos) {
            this.positionClick = pos;
        }
        
        @Override
        public void onClick(View v) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container_article, ArticleDetailFragment.newInstance(articleArrayList.get(positionClick)), ArticleDetailFragment.TAG_ARTICLE_DETAIL)
//                    .commit();
        }
    }
//
//    public void setData(ArrayList<Article> articleArrayList) {
//        this.articleArrayList = articleArrayList;
//        notifyDataSetChanged();
//    }
    
    static class AmenitiesHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        //        @BindView(R.id.txt_desc)
//        TextView txtDesc;
        @BindView(R.id.imgContent)
        ImageView imgContent;
        
        
        AmenitiesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
