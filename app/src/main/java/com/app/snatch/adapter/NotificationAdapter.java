package com.app.snatch.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.snatch.R;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    
    private ArrayList<String> notifcationList;
    private Context mContext;
    private FragmentManager fragmentManager;
    
    public static final int ITEM_TYPE_TEXT = 0;
    public static final int ITEM_TYPE_IMAGE = 1;
    
    
    public NotificationAdapter(ArrayList<String> notifcationList, FragmentManager fragmentManager) {
        this.notifcationList = notifcationList;
        this.fragmentManager = fragmentManager;
    }
    
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_TYPE_TEXT;
        } else {
            return ITEM_TYPE_IMAGE;
        }
    }
    
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        mContext = parent.getContext();
//        View view = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_notifcation, parent, false);
//        return new NotificationHolderText(view);
    
        if (viewType == ITEM_TYPE_TEXT) {
            View viewText = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_notifcation, parent, false);
            return new NotificationHolderText(viewText); // view holder for normal items
        } else if (viewType == ITEM_TYPE_IMAGE) {
            View viewImage = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notifcation_image, null);
            return new NotificationHolderImage(viewImage); // view holder for header items
        }
    
    
        return null;
    }
    
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    
//        final int itemType = getItemViewType(position);
//
//        if (itemType == ITEM_TYPE_NORMAL) {
//            ((MyNormalViewHolder)holder).bindData((MyModel)myData[position]);
//        } else if (itemType == ITEM_TYPE_HEADER) {
//            ((MyHeaderViewHolder)holder).setHeaderText((String)myData[position]);
//        }
    }
    
    
    @Override
    public int getItemCount() {
        return 2;
    }
    
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        
        ItemListener(int pos) {
            this.positionClick = pos;
        }
        
        @Override
        public void onClick(View v) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container_article, ArticleDetailFragment.newInstance(notifcationList.get(positionClick)), ArticleDetailFragment.TAG_ARTICLE_DETAIL)
//                    .commit();
        }
    }
//
//    public void setData(ArrayList<Article> notifcationList) {
//        this.notifcationList = notifcationList;
//        notifyDataSetChanged();
//    }
    
    static class NotificationHolderText extends RecyclerView.ViewHolder {
//        @BindView(R.id.txt_title)
//        TextView txtTitle;
//        @BindView(R.id.txt_desc)
//        TextView txtDesc;
//        @BindView(R.id.img_content)
//        ImageView imgContent;
        
        
        NotificationHolderText(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    
    static class NotificationHolderImage extends RecyclerView.ViewHolder {
//        @BindView(R.id.txt_title)
//        TextView txtTitle;
//        @BindView(R.id.txt_desc)
//        TextView txtDesc;
//        @BindView(R.id.img_content)
//        ImageView imgContent;
    
    
        NotificationHolderImage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
