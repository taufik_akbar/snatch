package com.app.snatch.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.app.snatch.fragment.MyListingSnatchFragment;
import com.app.snatch.utils.Constants;

import java.util.ArrayList;

public class MyListingTabAdapter extends FragmentStatePagerAdapter implements Constants {

    private ArrayList<String> types;

    public MyListingTabAdapter(FragmentManager fm, ArrayList<String> types) {
        super(fm);
        this.types = types;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment =  MyListingSnatchFragment.newInstance(MY_SNATCH);
                break;
            case 1:
                fragment =  MyListingSnatchFragment.newInstance(OWNED_LIST);
                break;
            case 2:
                fragment =  MyListingSnatchFragment.newInstance(DRAFT);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return types.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return types.get(position);
    }
}