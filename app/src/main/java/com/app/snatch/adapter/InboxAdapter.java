package com.app.snatch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.model.Message;
import com.app.snatch.utils.DateTimeHelper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.InboxHolder> {
    
    private ArrayList<Message> messageArrayList;
    private Context mContext;
    
    public InboxAdapter(ArrayList<Message> messageArrayList) {
        this.messageArrayList = messageArrayList;
    }
    
    
    @Override
    public InboxHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_inbox, parent, false);
        return new InboxHolder(view);
    }
    
    @Override
    public void onBindViewHolder(InboxHolder holder, int position) {
        Message data = messageArrayList.get(position);
        
        if (data != null) {
    
            String profPic = ApiConfig.BASE_URL_IMAGE + data.getUser().getImagePath().getImage();
            Glide.with(mContext).load(profPic).apply(new RequestOptions().centerCrop()).into(holder.imgProfile);
    
            System.out.println("MESSAGE INBOX ADAPTER : " + data.getMessage());
            holder.txtName.setText(data.getUser().getName());
            holder.txtTitle.setText(data.getSnatchName());
            holder.txtDesc.setText(data.getMessage());
            
            String time = DateTimeHelper.getTime(data.getCreated());
    
            System.out.println("TIME FORMAT : " + time);
            holder.txtTime.setText(time);
        }
    }
    
    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }
    
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        
        ItemListener(int pos) {
            this.positionClick = pos;
        }
        
        @Override
        public void onClick(View v) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container_article, ArticleDetailFragment.newInstance(messageArrayList.get(positionClick)), ArticleDetailFragment.TAG_ARTICLE_DETAIL)
//                    .commit();
        }
    }
//
//    public void setData(ArrayList<Article> messageArrayList) {
//        this.messageArrayList = messageArrayList;
//        notifyDataSetChanged();
//    }
    
    static class InboxHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtDesc)
        TextView txtDesc;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.imgProfile)
        CircleImageView imgProfile;
        
        
        InboxHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
