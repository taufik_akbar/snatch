package com.app.snatch.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.snatch.R;
import com.app.snatch.api.ApiConfig;
import com.app.snatch.model.ImagePath;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

/**
 * Created by taufik on 12/13/17.
 */

public class ImagePagerAdapter extends PagerAdapter {
    
//    int[] mResources = {
//            R.drawable.dummy_room,
//            R.drawable.img_snatch_list
//    };
    
    private ArrayList<ImagePath> imagePaths;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    
    public ImagePagerAdapter(Context mContext, ArrayList<ImagePath> imagePaths) {
        this.mContext = mContext;
        this.imagePaths = imagePaths;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    @Override
    public int getCount() {
        return imagePaths.size();
    }
    
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }
    
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_pager_image, container, false);
        
        ImageView imageView = itemView.findViewById(R.id.imgContent);
        
        String pathImage =  ApiConfig.BASE_URL_IMAGE + imagePaths.get(position).getImage();
        Glide.with(mContext).load(pathImage) // Uri of the picture
                .apply(new RequestOptions().centerCrop()).into(imageView);
//        imageView.setImageResource(mResources[position]);
        
        container.addView(itemView);
        
        return itemView;
    }
    
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
