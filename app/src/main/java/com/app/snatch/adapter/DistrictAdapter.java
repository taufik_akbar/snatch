package com.app.snatch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.app.snatch.R;
import com.app.snatch.model.District;
import com.app.snatch.model.bus.DistrictEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class DistrictAdapter extends RecyclerView.Adapter<DistrictAdapter.Holder> {
    
    private ArrayList<District> districtArraylist;
    private Context mContext;
    
    public DistrictAdapter(ArrayList<District> districtArraylist) {
        this.districtArraylist = districtArraylist;
    }
    
    
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_district, parent, false);
        return new Holder(view);
    }
    
    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        
        final District text = districtArraylist.get(position);
        holder.rbChoose.setText(text.getDistrict());
        
        holder.rbChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new DistrictEvent(text.getDistrict(), text.getDistrictId()));
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new DistrictEvent(text.getDistrict(), text.getDistrictId()));
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return districtArraylist.size();
    }
    
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        
        ItemListener(int pos) {
            this.positionClick = pos;
        }
        
        @Override
        public void onClick(View v) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container_article, ArticleDetailFragment.newInstance(districtArraylist.get(positionClick)), ArticleDetailFragment.TAG_ARTICLE_DETAIL)
//                    .commit();
        }
    }
//
//    public void setData(ArrayList<Article> districtArraylist) {
//        this.districtArraylist = districtArraylist;
//        notifyDataSetChanged();
//    }
    
    static class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.rbDistrict)
        RadioButton rbChoose;
        
        
        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
