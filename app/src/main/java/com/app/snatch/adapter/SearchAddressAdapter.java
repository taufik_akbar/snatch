package com.app.snatch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.snatch.R;
import com.app.snatch.model.bus.SearchAddressEvent;
import com.app.snatch.model.response.LocationResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Taufik Akbar on 01/03/2017.
 */

public class SearchAddressAdapter extends RecyclerView.Adapter<SearchAddressAdapter.Holder> {
    
    private ArrayList<LocationResponse.Data> districtArraylist;
    private Context mContext;
    
    public SearchAddressAdapter(ArrayList<LocationResponse.Data> districtArraylist) {
        this.districtArraylist = districtArraylist;
    }
    
    
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_address, parent, false);
        return new Holder(view);
    }
    
    @Override
    public void onBindViewHolder(Holder holder, final int position) {
    
        final LocationResponse.Data data = districtArraylist.get(position);
        
        final String address = data.getAddressComponents().get(1).getShortName();
        holder.txtTitle.setText(address);
        holder.txtAddress.setText(data.getFormattedAddress());
        
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new SearchAddressEvent(address, data.getAddressComponents().get(0).getShortName()));
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return districtArraylist.size();
    }
    
    
    private class ItemListener implements View.OnClickListener {
        
        private int positionClick;
        
        ItemListener(int pos) {
            this.positionClick = pos;
        }
        
        @Override
        public void onClick(View v) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container_article, ArticleDetailFragment.newInstance(districtArraylist.get(positionClick)), ArticleDetailFragment.TAG_ARTICLE_DETAIL)
//                    .commit();
        }
    }
//
    public void setData(ArrayList<LocationResponse.Data> districtArraylist) {
        System.out.println("RESYLT SIZE ARRAY : " + districtArraylist.size());
        this.districtArraylist = districtArraylist;
        notifyDataSetChanged();
    }
    
    static class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtAddress)
        TextView txtAddress;
        
        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
